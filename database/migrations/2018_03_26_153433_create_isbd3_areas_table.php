<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIsbd3AreasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('isbd3_areas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('mathematicalData')->nullable();
            $table->string('statementOfMusicFormat')->nullable();
            $table->string('numbering')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('isbd3_areas');
    }
}
