<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIsbd6AreasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('isbd6_areas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ownTitleOfMonographicFeatureInSeries')->nullable();
            $table->string('parallelTitleOfMultipartySeries')->nullable();
            $table->string('otherTitleInformationForSerial')->nullable();
            $table->string('declarationOfLiabilityForSerial')->nullable();
            $table->string('internationalStandardNumberOfSerial')->nullable();
            $table->string('numberingWithinSerial')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('isbd6_areas');
    }
}
