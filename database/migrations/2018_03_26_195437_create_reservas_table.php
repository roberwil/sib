<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservas', function (Blueprint $table) {

            $table->increments('id');
            $table->boolean('state');
            $table->date('startDate');
            $table->date('endDate');

            $table->integer('reader')->unsigned();
            $table->integer('exemplary')->unsigned();

            $table->foreign('reader')->references('id')->on('leitores');
            $table->foreign('exemplary')->references('id')->on('exemplars');

            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservas');
    }
}
