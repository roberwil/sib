<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAndarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('andars', function (Blueprint $table) {
            $table->increments('id');
            $table->string('description');
            $table->integer('nLines');
            $table->integer('libraryId')->unsigned();
            $table->foreign('libraryId')->references('id')->on('bibliotecas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('andars');
    }
}
