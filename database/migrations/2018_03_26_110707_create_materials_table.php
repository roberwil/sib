<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('materials', function (Blueprint $table) {
            $table->increments('id');
            $table->string('classification');

            $table->string('title');
            $table->string('authors');
            $table->string('edition');
            $table->string('editionYear');
            $table->string('publisher');
            $table->string('publishingYear');

            $table->integer('type');
            $table->integer('nExemplary');
            $table->integer('registeredBy')->unsigned()->nullable();
            $table->foreign('registeredBy')->references('id')->on('bibliotecarios');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('materials');
    }
}
