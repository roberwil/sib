<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmprestimosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emprestimos', function (Blueprint $table) {

            $table->increments('id');
            $table->boolean('state');
            $table->boolean('fined');
            $table->integer('fineValue')->nullable();
            $table->date('startDate');
            $table->date('endDate');


            $table->integer('reader')->unsigned();
            $table->integer('librarian')->unsigned();
            $table->integer('exemplary')->unsigned();

            $table->foreign('reader')->references('id')->on('leitores');
            $table->foreign('librarian')->references('id')->on('bibliotecarios');
            $table->foreign('exemplary')->references('id')->on('exemplars');

            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emprestimos');
    }
}
