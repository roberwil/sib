<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIsbd8AreasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('isbd8_areas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('resourceIdentifier')->nullable();
            $table->string('keyTitle')->nullable();
            $table->string('termsAvailability')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('isbd8_areas');
    }
}
