<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCatalogosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('catalogos', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('material')->unsigned();
            $table->integer('area1')->unsigned();
            $table->integer('area2')->unsigned();
            $table->integer('area3')->unsigned();
            $table->integer('area4')->unsigned();
            $table->integer('area5')->unsigned();
            $table->integer('area6')->unsigned();
            $table->integer('area7')->unsigned();
            $table->integer('area8')->unsigned();

            $table->foreign('material')->references('id')->on('materials');
            $table->foreign('area1')->references('id')->on('isbd1_areas');
            $table->foreign('area2')->references('id')->on('isbd2_areas');
            $table->foreign('area3')->references('id')->on('isbd3_areas');
            $table->foreign('area4')->references('id')->on('isbd4_areas');
            $table->foreign('area5')->references('id')->on('isbd5_areas');
            $table->foreign('area6')->references('id')->on('isbd6_areas');
            $table->foreign('area7')->references('id')->on('isbd7_areas');
            $table->foreign('area8')->references('id')->on('isbd8_areas');

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('catalogos');
    }
}
