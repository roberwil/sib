<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIsbd5AreasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('isbd5_areas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('specificationOfMaterialAndExtension')->nullable();
            $table->string('otherPhysicalDetails')->nullable();
            $table->string('dimensions')->nullable();
            $table->string('declarationOfAccompanyingMaterial')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('isbd5_areas');
    }
}
