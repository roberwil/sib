<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIsbd4AreasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('isbd4_areas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('placeOfPublication')->nullable();
            $table->string('nameOfPublisher')->nullable();
            $table->date('dateOfPublication')->nullable();
            $table->string('placeOfPrinting')->nullable();
            $table->string('printerName')->nullable();
            $table->date('dateOfPrinting')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('isbd4_areas');
    }
}
