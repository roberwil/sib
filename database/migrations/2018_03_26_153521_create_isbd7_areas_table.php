<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIsbd7AreasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('isbd7_areas', function (Blueprint $table) {

            $table->increments('id');

            $table->string('contentMediaTypeArea')->nullable();
            $table->string('titleDeclarationAreaResponsibility')->nullable();
            $table->string('editionAreaBibliographicHistoryResource')->nullable();
            $table->string('materialTypeResourceSpecificArea')->nullable();
            $table->string('publicationArea')->nullable();
            $table->string('physicalDescriptionArea')->nullable();

            $table->string('seriesArea')->nullable();
            $table->string('content')->nullable();
            $table->string('resourceIdentifierAvailabilityAreaTerms')->nullable();
            $table->string('questionPartIterationBaseDescription')->nullable();
            $table->string('otherNotes')->nullable();
            $table->string('copyingHands')->nullable();

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('isbd7_areas');
    }
}
