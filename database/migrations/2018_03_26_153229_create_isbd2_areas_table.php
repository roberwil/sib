<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIsbd2AreasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('isbd2_areas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('editionStatement')->nullable();
            $table->string('parallelEditionStatement')->nullable();
            $table->string('declarationOfResponsibilityForEdition')->nullable();
            $table->string('additionalEditionInstruction')->nullable();
            $table->string('statementOfResponsibilityAfter')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('isbd2_areas');
    }
}
