<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIsbd1AreasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('isbd1_areas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('appropriateTitle')->nullable();
            $table->string('parallelTitle')->nullable();
            $table->string('otherTitleInformation')->nullable();
            $table->string('declarationOfResponsibility')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('isbd1_areas');
    }
}
