<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExemplarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exemplars', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('number');
            $table->integer('label');
            $table->string('callNumber');
            $table->boolean('available');
            $table->integer('shelf')->unsigned()->nullable();
            $table->integer('material')->unsigned();
            $table->integer('librarian')->unsigned();

            $table->foreign('librarian')->references('id')->on('bibliotecarios');
            $table->foreign('material')->references('id')->on('materials');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exemplars');
    }
}
