<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $admin = new \App\Admin;

        $admin->fullname = 'Roberto Ribeiro';
        $admin->username = 'roberwil';
        $admin->email = 'rober.will@hotmail.com';
        $admin->password = Hash::make('!password');

        $admin->save();

    }
}
