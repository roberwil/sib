

( function ( $ ) {

    "use strict";

    /** track onchange when choosing floor - registering or editing bookcase */

    $('.eb-change-lines').change(function () {

        var id = $('.eb-change-lines').val();

        $.ajax(
            {
                type: 'GET',
                url: '/helper/lines/' + id,
                success: function (response) {

                    var options = '';
                    for (var i = 1; i <= response.maxLine; i++) {
                        options += '<option value="' + i + '">' + i + '</option>';
                    }

                    $('.eb-lines').html(options);

                }
            },
            this
        );

    });

    $('#rb-change-lines').change(function () {
        //alert('something changed, bitch');
        var id = $('#rb-change-lines').val();
        $.ajax(
            {
                type: 'GET',
                url: '/helper/lines/' + id,
                success: function (response) {

                    var options = '';
                    for (var i = 1; i <= response.maxLine; i++) {
                        options += '<option value="' + i + '">' + i + '</option>';
                    }

                    $('#rb-lines').html(options);

                }
            },
            this
        );
    });

    /** track onchange on element to add new cdd */

    $('.remove-cdd').click(function (event) {
        event.preventDefault();
        alert('message');
    })

    $('#add-cdd-level-1').click(function () {
        $.ajax(
            {
                type: 'GET',
                url: '/helper/get-cdd/1',
                success: function (response) {
                    // block user to add more than 3 cdds
                    if ($('#cdd-area > select').length == 3) {
                        $('#restart-cdd').html('Atingiu o número máximo. <a href="/catalog/register/new/view">Clique aqui para recomeçar</a> ');
                        return;
                    }
                    // enable 'Seguinte" btn
                    $('#classify-btn').removeAttr('disabled');
                    // delete warning box
                    $('#cdd-not-added').remove();
                    //<span class="btn btn-outline-dark btn-sm remove-cdd"> Remover </span> &nbsp;
                    var html = '<label class="font-weight-bold"> Nível 1 </label>'
                    html += '<select name="cdd[]" class="form-control">';

                    for (var key in response) {
                        var option = '<option value="' + key + '">' + response[key] + '</option>';
                        html += option;
                    }

                    html += '</select>';
                    $('#cdd-area').append(html);
                },
                error: function () {
                    console.log('something went wrong');
                }

            },
            this
        );
    });

    $('#add-cdd-level-2').click(function () {
        $.ajax(
            {
                type: 'GET',
                url: '/helper/get-cdd/2',
                dataType: 'json',
                success: function (response) {

                    //block user to add more than 3 cdds
                    if ($('#cdd-area > select').length == 3) {
                        $('#restart-cdd').html('Atingiu o número máximo. <a href="/catalog/register/new/view">Clique aqui para recomeçar</a> ');
                        return;
                    }

                    // enable 'Seguinte" btn
                    $('#classify-btn').removeAttr('disabled');
                    // delete warning box
                    $('#cdd-not-added').remove();

                    var html = '<label class="font-weight-bold"> Nível 2 </label>'
                        html += '<select name="cdd[]" class="form-control">';

                    for (var key in response) {
                        var option = '<option value="' + key + '">' + response[key] + '</option>';
                        html += option;
                    }

                    html += '</select>';
                    $('#cdd-area').append(html);

                },
                error: function () {
                    console.log('something went wrong');
                }

            },
            this
        );
    });

    $('#add-cdd-level-3').click(function () {
        $.ajax(
            {
                type: 'GET',
                url: '/helper/get-cdd/3',
                dataType: 'json',
                success: function (response) {

                    //block user to add more than 3 cdds
                    if ($('#cdd-area > select').length == 3) {
                        $('#restart-cdd').html('Atingiu o número máximo. <a href="/catalog/register/new/view">Clique aqui para recomeçar</a> ');
                        return;
                    }

                    // enable 'Seguinte" btn
                    $('#classify-btn').removeAttr('disabled');
                    // delete warning box
                    $('#cdd-not-added').remove();

                    var html = '<label class="font-weight-bold"> Nível 3 </label>'
                    html += '<select name="cdd[]" class="form-control">';

                    for (var key in response) {
                        var option = '<option value="' + key + '">' + response[key] + '</option>';
                        html += option;
                    }

                    html += '</select>';
                    $('#cdd-area').append(html);

                },
                error: function () {
                    console.log('something went wrong');
                }

            },
            this
        );
    });

    /** edit exemplaries */

    $('#ee-floors').change(function () {
        var id = $('#ee-floors').val();
        //alert('something changed, bitch: ' + id);
        $.ajax(
            {
                type: 'GET',
                url: '/helper/exemplary-location/1/' + id,
                success: function (response) {
                    console.log('something');
                    var options = '';
                    for (var i = 1; i <= response.maxLine; i++) {
                        options += '<option value="' + i + '">' + i + '</option>';
                    }

                    $('#ee-lines').html(options);

                },
                error: function () {
                    console.log('error');
                }
            },
            this
        );
    });

    $('#ee-lines').change(function () {
        //alert('something changed, bitch');
        var id = $('#ee-lines').val();
        var helperId = $('#ee-floors').val();
        $.ajax(
            {
                type: 'GET',
                url: '/helper/exemplary-location/2/' + id + '/' + helperId,
                success: function (response) {
                    console.log(response);
                    var options = '';

                    for (var key in response) {
                        options += '<option value="' + key + '">' + response[key] + '</option>';
                    }

                    $('#ee-bookcases').html(options);

                },
                error: function () {
                    console.log('error again');
                }
            },
            this
        );
    });

    $('#ee-bookcases').change(function () {
        //alert('something changed, bitch');
        var id = $('#ee-bookcases').val();
        $.ajax(
            {
                type: 'GET',
                url: '/helper/exemplary-location/3/' + id,
                success: function (response) {
                    console.log(response);
                    var options = '';
                    alert(id);
                    for (var key in response) {
                        options += '<option value="' + key + '">' + response[key] + '</option>';
                    }

                    $('#ee-shelves').html(options);

                },
                error: function () {
                    console.log('error again');
                }
            },
            this
        );
    });

    /** check book and user availabiblity when borrowing books */

    $('#book-unavailable').hide();
    $('#book-available').hide();
    $('#user-unavailable').hide();
    $('#user-available').hide();

    $('#bb-book-btn').click(function () {
        var id = $('#bb-book').val();
        $.ajax(
            {
                type: 'GET',
                url: '/helper/check/book/' + id,
                success: function (response) {
                    console.log(response);
                    if (response.cool == 1){
                        $('#book-available').show();
                        $('#book-unavailable').hide();
                    } else {
                        $('#book-unavailable').show();
                        $('#book-available').hide();
                    }
                },
                error: function () {
                    console.log('error processing "check book availability" request');
                    $('#book-unavailable').show();
                    $('#book-available').hide();
                }
            },
            this);
    });

    $('#bb-user-btn').click(function () {
        var id = $('#bb-user').val();
        $.ajax(
            {
                type: 'GET',
                url: '/helper/check/user/' + id,
                success: function (response) {
                    console.log(response);
                    if (response.cool == 1){
                        $('#user-available').show();
                        $('#user-unavailable').hide();
                    } else {
                        $('#user-unavailable').show();
                        $('#user-available').hide();
                    }
                },
                error: function () {
                    console.log('error processing "check book availability" request');
                    $('#user-unavailable').show();
                    $('#user-available').hide();
                }
            },
            this);
    });

    /** other functionalities */

    $('[data-toggle="popover"]').popover();

    $('.popover-dismiss').popover({
        trigger: 'focus'
    })

    /** theme feature code */

    jQuery( '#vmap' ).vectorMap( {
        map: 'world_en',
        backgroundColor: null,
        color: '#ffffff',
        hoverOpacity: 0.7,
        selectedColor: '#1de9b6',
        enableZoom: true,
        showTooltip: true,
        values: sample_data,
        scaleColors: [ '#1de9b6', '#03a9f5' ],
        normalizeFunction: 'polynomial'
    } );

} )( jQuery );
