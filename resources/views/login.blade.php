<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title> SIB | ISPTEC </title>
    <meta name="description" content="Sufee Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="shortcut icon" href="favicon.ico">

    <link rel="stylesheet" href="/css/theme/normalize.css">
    <link href="/css/app.css" rel="stylesheet" type="text/css">
    {{--<link rel="stylesheet" href="/css/theme/bootstrap.css">--}}
    <link rel="stylesheet" href="/css/theme/font-awesome.min.css">
    <link rel="stylesheet" href="/css/theme/themify-icons.css">
    <link rel="stylesheet" href="/css/theme/flag-icon.min.css">
    <link rel="stylesheet" href="/css/theme/cs-skin-elastic.css">
    <!-- <link rel="stylesheet" href="assets/css/bootstrap-select.less"> -->
    <link rel="stylesheet" href="/css/theme/main.css">
    <link rel="stylesheet" href="/css/theme/lib/vector-map/jqvmap.min.css">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

    <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.min.js"></script>

</head>
<body class="bg-dark">

<div class="sufee-login d-flex align-content-center flex-wrap">
    <div class="container">
        <div class="login-content">
            <div class="login-logo">
                <a href="index.html">
                    <h1> SIB | ISPTEC </h1>
                </a>
            </div>
            <div class="login-form">
                {!! Form::open(['url' => '/login']) !!}

                    @if(isset($error))
                    <div class="alert-danger">
                        <span class="text-center"> {{ $error or '' }} </span>
                    </div>
                    @endif

                    {{ Form::label('username', 'Nome de Utilizador', ['class' => 'font-weight-bold'])  }}
                    {{ Form::text('username', '', ['class' => 'form-control', 'placeholder' => 'Insira aqui o seu nome de utilizador', 'required']) }}

                    {{ Form::label('password', 'Palavra-Passe', ['class' => 'font-weight-bold'])  }}
                    {{ Form::password('password', ['class' => 'form-control', 'placeholder' => 'Insira aqui a sua palavra-passe', 'required']) }}

                    {{ Form::submit('Entrar', ['class' => 'btn btn-success btn-flat m-b-30 m-t-30']) }}

                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>


<script src="/js/theme/vendor/jquery-2.1.4.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"></script>
<script src="/js/theme/plugins.js"></script>
<script src="/js/theme/main.js"></script>



</body>
</html>