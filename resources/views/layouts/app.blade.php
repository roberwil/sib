<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title> SIB | ISPTEC </title>
    <meta name="description" content="Sufee Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="shortcut icon" href="favicon.ico">

    <link rel="stylesheet" href="/css/theme/normalize.css">
    <link href="/css/app.css" rel="stylesheet" type="text/css">
    {{--<link rel="stylesheet" href="/css/theme/bootstrap.css">--}}
    <link rel="stylesheet" href="/css/theme/font-awesome.min.css">
    <link rel="stylesheet" href="/css/theme/themify-icons.css">
    <link rel="stylesheet" href="/css/theme/flag-icon.min.css">
    <link rel="stylesheet" href="/css/theme/cs-skin-elastic.css">
    <!-- <link rel="stylesheet" href="assets/css/bootstrap-select.less"> -->
    <link rel="stylesheet" href="/css/theme/main.css">
    <link rel="stylesheet" href="/css/theme/lib/vector-map/jqvmap.min.css">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

    <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.min.js"></script>

</head>
<body>

<!-- NAVIGATION PANEL -->
<aside id="left-panel" class="left-panel">
    <nav class="navbar navbar-expand-sm navbar-default">

        <div class="navbar-header">

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fa fa-bars"></i>
            </button>
            <a class="navbar-brand" href="/"><h3> SIB | ISPTEC </h3></a>
            <a class="navbar-brand hidden" href="/"><h4> S </h4></a>

        </div>

        @if(session('profile') == 'admin')
            @include('navs.admin')
        @elseif(session('profile') == 'librarian')
            @include('navs.librarian')
        @else
            @include('navs.userbib')
        @endif

    </nav>
</aside><!-- /#left-panel -->


<!-- RIGHT PANEL -->
<div id="right-panel" class="right-panel">

    <!-- Header-->
    <header id="header" class="header">

        <div class="header-menu">

            <div class="col-sm-7">
                <a id="menuToggle" class="menutoggle pull-left"><i class="fa fa fa-tasks"></i></a>
                <h4> {{ session('fullname') }}</h4>
                <p class="text-muted"> {{ \App\Helpers\Sib::resolveProfile(session('profile'), session('user')) }} </p>
            </div>

            <div class="col-sm-5">
                <div class="user-area dropdown float-right">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img class="user-avatar rounded-circle" src="/images/admin.png" alt="User Avatar">
                    </a>
                    <div class="user-menu dropdown-menu">
                        <a class="nav-link" href="/user/edit-profile"><i class="fa fa-cog"></i> &nbsp; Editar Perfil</a>
                        <a class="nav-link" href="/logout"><i class="fa fa-power-off"></i> &nbsp; Sair</a>
                    </div>
                </div>

            </div>
        </div>

    </header><!-- /header -->
    <!-- Header-->

    <div class="breadcrumbs">
        <div class="col-sm-8">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1 class="font-weight-bold"> @yield('page-description') </h1>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="page-header float-right">
                <a class="btn btn-outline-dark mt-2" href="{{ url()->current() }}">
                    <i class="fa fa-refresh"></i> &nbsp; Atualizar
                </a>
            </div>
        </div>
    </div>

    <div class="content mt-3">
        @yield('content')
    </div>

</div>
<!--/.col-->

 <!-- .content -->
</div><!-- /#right-panel -->

<!-- Right Panel -->

<!-- MODALS -->

<!-- REGISTER MODALS -->
@include('modals.register-admin')
@include('modals.register-librarian')
@include('modals.register-userbib')
@include('modals.register-group')
@include('modals.register-floor')
@include('modals.register-bookcase')
@include('modals.register-shelf')
@include('modals.register-exemplary')
<!-- EDIT MODALS -->
@include('modals.edit-admin')
@include('modals.edit-librarian')
@include('modals.edit-group')
@include('modals.edit-userbib')
@include('modals.config-edit-library')
@include('modals.edit-floor')
@include('modals.edit-bookcase')
@include('modals.edit-shelf')
@include('modals.edit-exemplary')
<!-- VIEW MODALS -->
@include('modals.view-group-details')
@include('modals.view-material-details')
<!-- End > Modals  -->


<script src="/js/theme/vendor/jquery-2.1.4.min.js"></script>
<script src="/js/theme/popper.min.js"></script>
<script src="/js/theme/plugins.js"></script>
<script src="/js/theme/main.js"></script>


<script src="/js/theme/lib/chart-js/Chart.bundle.js"></script>
<script src="/js/theme/dashboard.js"></script>
<script src="/js/theme/widgets.js"></script>
<script src="/js/theme/lib/vector-map/jquery.vmap.js"></script>
<script src="/js/theme/lib/vector-map/jquery.vmap.min.js"></script>
<script src="/js/theme/lib/vector-map/jquery.vmap.sampledata.js"></script>
<script src="/js/theme/lib/vector-map/country/jquery.vmap.world.js"></script>
<script src="/js/sib.js"></script>

</body>
</html>
