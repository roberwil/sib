@extends('layouts.app')
@section('page-description', 'Catalogação | Passo 1: Informações Básicas')

@section('content')

    <div class="col-md-12 mb-3">
        <button class="btn btn-outline-secondary dropdown-toggle"
                type="button"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"> Adicionar CDD</button>
        <div class="dropdown-menu">
            <span id="add-cdd-level-1" class="dropdown-item" href=""> <i class="fa fa-plus-square"></i> &nbsp; Nível 1 </span>
            <span id="add-cdd-level-2" class="dropdown-item" href=""> <i class="fa fa-plus-square"></i> &nbsp; Nível 2 </span>
            <span id="add-cdd-level-3" class="dropdown-item" href=""> <i class="fa fa-plus-square"></i> &nbsp; Nível 3 </span>
        </div>
        &nbsp; <span id="restart-cdd" class="text-danger"> <a class="text-muted" href="/catalog/register/new/view">Clique aqui para recomeçar</a> </span>
    </div>

    <div class="col-md-11 card card-body">
        {!! Form::open(['url' => '/catalog/register/new/1']) !!}

            <div class="form-row">
                <div class="col">
                    {{ Form::label('title', 'Título', ['class' => 'font-weight-bold']) }}
                    {{ Form::text('title', '', ['class' => 'form-control', 'placeholder' => 'Insira aqui o titulo próprio', 'required' ]) }}
                </div>
                <div class="col">
                    {{ Form::label('authors', 'Autor(es)', ['class' => 'font-weight-bold']) }}
                    {{ Form::text('authors', '', ['class' => 'form-control', 'placeholder' => 'Nome do autor no formato Sobrenome, Outros nomes', 'required']) }}
                </div>
            </div>

            <div class="form-row">
                <div class="col">
                    {{ Form::label('edition', 'Edição', ['class' => 'font-weight-bold']) }}
                    {{ Form::text('edition', '', ['class' => 'form-control', 'placeholder' => 'Edição no formato 1 ed.,  2 ed., etc', 'required' ]) }}
                </div>
                <div class="col">
                    {{ Form::label('editionYear', 'Ano de Edição', ['class' => 'font-weight-bold']) }}
                    {{ Form::text('editionYear', '', ['class' => 'form-control', 'placeholder' => 'Insira aqui o ano da edição', 'required' ]) }}
                </div>
            </div>

            <div class="form-row">
                <div class="col">
                    {{ Form::label('publisher', 'Editora', ['class' => 'font-weight-bold']) }}
                    {{ Form::text('publisher', '', ['class' => 'form-control', 'placeholder' => 'Nome da Editora', 'required' ]) }}
                </div>
                <div class="col">
                    {{ Form::label('publishingYear', 'Ano de Publicação', ['class' => 'font-weight-bold']) }}
                    {{ Form::text('publishingYear', '', ['class' => 'form-control', 'placeholder' => 'Ano de Publicação', 'required' ]) }}
                </div>
            </div>

            <div class="form-row">
                <div class="col">
                    {{ Form::label('type', 'Tipo de Material', ['class' => 'font-weight-bold'])  }}
                    {{ Form::select('type', \App\Helpers\Sib::getJsonData('type-of-materials', 1) , '', ['class' => 'form-control', 'required']) }}
                </div>
                <div class="col">
                    {{ Form::label('nExemplary', 'Número de exemplares', ['class' => 'font-weight-bold', 'required']) }}
                    {{ Form::number('nExemplary', 1, ['class' => 'form-control mb-3', 'min' => '1' ]) }}
                </div>
            </div>

            {{ Form::label('cdd', 'Claassificação (adicione até 3 CDDs)', ['class' => 'font-weight-bold']) }}
            <div id="cdd-area" class="form-group"> </div>
            <label class="alert alert-warning" id="cdd-not-added">
                Nenhuma CDD adicionada! <br>
                <span class="text-muted">Clique no botão "Adicionar CDD" para o fazer.</span>
            </label>  <br>

        {{ Form::submit('Seguinte', ['class' => 'btn btn-primary mb-5', 'id' => 'classify-btn' ,'disabled']) }}
        {!! Form::close() !!}
    </div>
@endsection