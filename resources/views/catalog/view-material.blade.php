@extends('layouts.app')
@section('page-description', 'Ver Materiais Catalogados')
@section('content')

    <div class="col-md-12">

        <table class="table">
            <thead class="thead-dark">
            <tr>
                <th scope="col"> ID  </th>
                <th scope="col"> Autor(es) </th>
                <th scope="col"> Título </th>
                <th scope="col"> Mais Informações </th>
                <th scope="col"> Opções </th>
            </tr>
            </thead>

            <tbody>

            @if(count($materials) === 0)
                <tr class="alert alert-dark">
                    <td class="text-center font-weight-bold" colspan="6"> Sem itens </td>
                </tr>
            @endif

            @foreach($materials as $material)
                <tr>
                    <td> {{ $material->id  }} </td>
                    <td> {{ $material->authors }} </td>
                    <td> {{ $material->title }} </td>
                    <td> <a href="" data-toggle="modal" data-target="#view-material-detail-{{$material->id}}"><i class="fa fa-eye"></i></> </td>

                    <td class="dropdown">
                        <div class="row">
                            <div class="col-md-10">
                                <button class="btn btn-outline-secondary btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Opções</button>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href=""> <i class="fa fa-eye"></i> &nbsp; Ver Catálogo </a>
                                    <a class="dropdown-item" href="/catalog/view/exemplary/{{$material->id}}"> <i class="fa fa-eye"></i> &nbsp; Ver Exemplares </a>
                                    <a class="dropdown-item" href="/catalog/view/edit-material/{{$material->id}}"> <i class="fa fa-edit"></i> &nbsp; Editar </a>
                                    <a class="dropdown-item" href=""><i class="fa fa-remove"></i> &nbsp; Eliminar </a>
                                </div>
                            </div>
                        </div>
                    </td>

                </tr>
            @endforeach
            </tbody>
        </table>

    </div>

@endsection

