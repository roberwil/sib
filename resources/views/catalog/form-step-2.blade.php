@extends('layouts.app')
@section('page-description', 'Catalogação | Passo 2: Descrição Bibliográfica')

@section('content')

    <div class="col-md-12">

        {!! Form::open(['url' => '/catalog/register/new/2']) !!}

            <!-- AREA 1 -->
            <p class="col-md-10">
                <button class="btn btn-outline-dark btn-sm" type="button" data-toggle="collapse" data-target="#area-1" aria-expanded="false" aria-controls="collapseExample">
                    Mostrar | Esconder
                </button>
                &nbsp; <span class="font-weight-bold"> Área 1: </span>  <span class="text-muted">Título e declaração da área de responsabilidade</span>
            </p>

            <div class="collapse" id="area-1">
                <div class="card card-body">

                    {{ Form::label('label', 'Título apropriado', ['class' => 'font-weight-bold']) }}
                    {{ Form::text('area1[]', '', ['class' => 'form-control mb-3', 'placeholder' => 'Insira aqui o titulo próprio' ]) }}

                    {{ Form::label('label', 'Título Paralelo', ['class' => 'font-weight-bold']) }}
                    {{ Form::text('area1[]', '', ['class' => 'form-control mb-3', 'placeholder' => 'Insira aqui o título paralelo' ]) }}

                    {{ Form::label('label', 'Outras informações do título', ['class' => 'font-weight-bold']) }}
                    {{ Form::text('area1[]', '', ['class' => 'form-control mb-3', 'placeholder' => 'Outras informações' ]) }}

                    {{ Form::label('', 'Declaração de responsabilidade', ['class' => 'font-weight-bold']) }}
                    {{ Form::text('area1[]', '', ['class' => 'form-control mb-3', 'placeholder' => 'Declaração de responsabilidade' ]) }}

                </div>
            </div>

            <!-- AREA 2 -->
            <p class="col-md-10">
                <button class="btn btn-outline-dark btn-sm" type="button" data-toggle="collapse" data-target="#area-2" aria-expanded="false" aria-controls="collapseExample">
                    Mostrar | Esconder
                </button>
                &nbsp; <span class="font-weight-bold"> Área 2: </span>
                <span class="text-muted"> Edição </span>
            </p>

            <div class="collapse" id="area-2">
                <div class="card card-body">

                    {{ Form::label('label', 'Declaração de edição', ['class' => 'font-weight-bold']) }}
                    {{ Form::text('area2[]', '', ['class' => 'form-control mb-3', 'placeholder' => 'Declaração de edição' ]) }}

                    {{ Form::label('label', 'Declaração de edição paralela', ['class' => 'font-weight-bold']) }}
                    {{ Form::text('area2[]', '', ['class' => 'form-control mb-3', 'placeholder' => 'Declaração de edição paralela' ]) }}

                    {{ Form::label('label', 'Declaração de responsabilidade relativa à edição', ['class' => 'font-weight-bold']) }}
                    {{ Form::text('area2[]', '', ['class' => 'form-control mb-3', 'placeholder' => 'eclaração de responsabilidade relativa à edição' ]) }}

                    {{ Form::label('label', 'Instrução adicional de edição', ['class' => 'font-weight-bold']) }}
                    {{ Form::text('area2[]', '', ['class' => 'form-control mb-3', 'placeholder' => 'Instrução adicional de edição' ]) }}

                    {{ Form::label('label', 'Declaração de responsabilidade após uma declaração adicional de edição', ['class' => 'font-weight-bold']) }}
                    {{ Form::text('area2[]', '', ['class' => 'form-control mb-3', 'placeholder' => 'Declaração de responsabilidade após uma declaração adicional de edição' ]) }}

                </div>
            </div>

            <!-- AREA 3 -->
            <p class="col-md-10">
                <button class="btn btn-outline-dark btn-sm" type="button" data-toggle="collapse" data-target="#area-3" aria-expanded="false" aria-controls="collapseExample">
                    Mostrar | Esconder
                </button>
                &nbsp; <span class="font-weight-bold"> Área 3: </span>
                <span class="text-muted"> Material ou tipo de área específica do recurso </span>
            </p>

            <div class="collapse" id="area-3">
                <div class="card card-body">

                    {{ Form::label('label', 'Dados matemáticos (recursos cartográficos)', ['class' => 'font-weight-bold']) }}
                    {{ Form::text('area3[]', '', ['class' => 'form-control mb-3', 'placeholder' => 'Dados matemáticos (recursos cartográficos)' ]) }}

                    {{ Form::label('label', 'Declaração de formato de música (música notada)', ['class' => 'font-weight-bold']) }}
                    {{ Form::text('area3[]', '', ['class' => 'form-control mb-3', 'placeholder' => 'Declaração de formato de música (música notada)' ]) }}

                    {{ Form::label('label', 'Numeração (Serials)', ['class' => 'font-weight-bold']) }}
                    {{ Form::text('area3[]', '', ['class' => 'form-control mb-3', 'placeholder' => 'Numeração (Serials)' ]) }}

                </div>
            </div>

            <!-- AREA 4 -->
            <p class="col-md-10">
                <button class="btn btn-outline-dark btn-sm" type="button" data-toggle="collapse" data-target="#area-4" aria-expanded="false" aria-controls="collapseExample">
                    Mostrar | Esconder
                </button>
                &nbsp; <span class="font-weight-bold"> Área 4: </span>
                <span class="text-muted"> Publicação, produção, distribuição, etc., área </span>
            </p>

            <div class="collapse" id="area-4">
                <div class="card card-body">

                    {{ Form::label('label', 'Local de publicação, produção e/ou distribuição', ['class' => 'font-weight-bold']) }}
                    {{ Form::text('area4[]', '', ['class' => 'form-control mb-3', 'placeholder' => 'Local de publicação, produção e/ou distribuição' ]) }}

                    {{ Form::label('label', 'Nome do editor, produtor e/ou distribuidor', ['class' => 'font-weight-bold']) }}
                    {{ Form::text('area4[]', '', ['class' => 'form-control mb-3', 'placeholder' => 'Alguma informação aqui' ]) }}

                    {{ Form::label('label', 'Data de publicação, produção e/ou distribuição', ['class' => 'font-weight-bold']) }}
                    {{ Form::date('area4[]', Carbon\Carbon::now(), ['class' => 'form-control mb-3', 'placeholder' => 'Nome do editor, produtor e/ou distribuidor' ]) }}

                    {{ Form::label('label', 'Lugar de impressão ou fabricação', ['class' => 'font-weight-bold']) }}
                    {{ Form::text('area4[]', '', ['class' => 'form-control mb-3', 'placeholder' => 'Lugar de impressão ou fabricação' ]) }}

                    {{ Form::label('label', 'Nome da impressora ou fabricante', ['class' => 'font-weight-bold']) }}
                    {{ Form::text('area4[]', '', ['class' => 'form-control mb-3', 'placeholder' => 'Nome da impressora ou fabricante' ]) }}

                    {{ Form::label('label', 'Data de impressão ou fabrico', ['class' => 'font-weight-bold']) }}
                    {{ Form::date('area4[]', Carbon\Carbon::now(), ['class' => 'form-control mb-3', 'placeholder' => 'Data de impressão ou fabrico' ]) }}

                </div>
            </div>

            <!-- AREA 5 -->
            <p class="col-md-10">
                <button class="btn btn-outline-dark btn-sm" type="button" data-toggle="collapse" data-target="#area-5" aria-expanded="false" aria-controls="collapseExample">
                    Mostrar | Esconder
                </button>
                &nbsp; <span class="font-weight-bold"> Área 5: </span>
                <span class="text-muted"> Descrição física </span>
            </p>

            <div class="collapse" id="area-5">
                <div class="card card-body">

                    {{ Form::label('label', 'Designação específica de material e extensão', ['class' => 'font-weight-bold']) }}
                    {{ Form::text('area5[]', '', ['class' => 'form-control mb-3', 'placeholder' => 'Designação específica de material e extensão' ]) }}

                    {{ Form::label('label', 'Outros detalhes físicos', ['class' => 'font-weight-bold']) }}
                    {{ Form::text('area5[]', '', ['class' => 'form-control mb-3', 'placeholder' => 'Outros detalhes físicos' ]) }}

                    {{ Form::label('label', 'Dimensões', ['class' => 'font-weight-bold']) }}
                    {{ Form::text('area5[]', '', ['class' => 'form-control mb-3', 'placeholder' => 'Dimensões' ]) }}

                    {{ Form::label('label', 'Declaração de material acompanhante', ['class' => 'font-weight-bold']) }}
                    {{ Form::text('area5[]', '', ['class' => 'form-control mb-3', 'placeholder' => 'Declaração de material acompanhante' ]) }}

                </div>
            </div>

            <!-- AREA 6 -->
            <p class="col-md-10">
                <button class="btn btn-outline-dark btn-sm" type="button" data-toggle="collapse" data-target="#area-6" aria-expanded="false" aria-controls="collapseExample">
                    Mostrar | Esconder
                </button>
                &nbsp; <span class="font-weight-bold"> Área 6: </span>
                <span class="text-muted"> Recursos monográficos de 6 Séries e multipartes </span>
            </p>

            <div class="collapse" id="area-6">
                <div class="card card-body">

                    {{ Form::label('label', 'Título próprio de um recurso monográfico em série ou multiparte', ['class' => 'font-weight-bold']) }}
                    {{ Form::text('area6[]', '', ['class' => 'form-control mb-3', 'placeholder' => 'Título próprio de um recurso monográfico em série ou multiparte' ]) }}

                    {{ Form::label('label', 'Título paralelo de uma série ou recurso monográfico multipartes', ['class' => 'font-weight-bold']) }}
                    {{ Form::text('area6[]', '', ['class' => 'form-control mb-3', 'placeholder' => 'Alguma informação aqui' ]) }}

                    {{ Form::label('label', 'Outras informações de título de um recurso monográfico de série ou multiparte', ['class' => 'font-weight-bold']) }}
                    {{ Form::text('area6[]', '', ['class' => 'form-control mb-3', 'placeholder' => 'Título paralelo de uma série ou recurso monográfico multipartes' ]) }}

                    {{ Form::label('label', 'Declaração de responsabilidade relativa a um recurso monográfico em série ou multiparte', ['class' => 'font-weight-bold']) }}
                    {{ Form::text('area6[]', '', ['class' => 'form-control mb-3', 'placeholder' => 'Declaração de responsabilidade relativa a um recurso monográfico em série ou multiparte' ]) }}

                    {{ Form::label('label', 'Número padrão internacional de um recurso monográfico em série ou multiparte', ['class' => 'font-weight-bold']) }}
                    {{ Form::text('area6[]', '', ['class' => 'form-control mb-3', 'placeholder' => 'Número padrão internacional de um recurso monográfico em série ou multiparte' ]) }}

                    {{ Form::label('label', 'Numeração dentro de um recurso monográfico de série ou multiparte', ['class' => 'font-weight-bold']) }}
                    {{ Form::text('area6[]', '', ['class' => 'form-control mb-3', 'placeholder' => 'Numeração dentro de um recurso monográfico de série ou multiparte' ]) }}

                </div>
            </div>

            <!-- AREA 7 -->
            <p class="col-md-10">
                <button class="btn btn-outline-dark btn-sm" type="button" data-toggle="collapse" data-target="#area-7" aria-expanded="false" aria-controls="collapseExample">
                    Mostrar | Esconder
                </button>
                &nbsp; <span class="font-weight-bold"> Área 7: </span>
                <span class="text-muted"> Notas </span>
            </p>

            <div class="collapse" id="area-7">
                <div class="card card-body">

                    {{ Form::label('label', 'Formulário de conteúdo e área do tipo de mídia e para tipos especiais de material', ['class' => 'font-weight-bold']) }}
                    {{ Form::text('area7[]', '', ['class' => 'form-control mb-3', 'placeholder' => 'Escreva aqui a nota' ]) }}

                    {{ Form::label('label', 'Título e a declaração da área de responsabilidade', ['class' => 'font-weight-bold']) }}
                    {{ Form::text('area7[]', '', ['class' => 'form-control mb-3', 'placeholder' => 'Escreva aqui a nota' ]) }}

                    {{ Form::label('label', 'Área de edição e a história bibliográfica do recurso', ['class' => 'font-weight-bold']) }}
                    {{ Form::text('area7[]', '', ['class' => 'form-control mb-3', 'placeholder' => 'Escreva aqui a nota' ]) }}

                    {{ Form::label('label', 'Material ou tipo de área específica do recurso', ['class' => 'font-weight-bold']) }}
                    {{ Form::text('area7[]', '', ['class' => 'form-control mb-3', 'placeholder' => 'Escreva aqui a nota' ]) }}

                    {{ Form::label('label', 'Publicação, produção, distribuição, etc., área', ['class' => 'font-weight-bold']) }}
                    {{ Form::text('area7[]', '', ['class' => 'form-control mb-3', 'placeholder' => 'Escreva aqui a nota' ]) }}

                    {{ Form::label('label', 'Área de descrição física', ['class' => 'font-weight-bold']) }}
                    {{ Form::text('area7[]', '', ['class' => 'form-control mb-3', 'placeholder' => 'Escreva aqui a nota' ]) }}

                    {{ Form::label('label', 'Área de recursos monográficos de séries e multipartes', ['class' => 'font-weight-bold']) }}
                    {{ Form::text('area7[]', '', ['class' => 'form-control mb-3', 'placeholder' => 'Escreva aqui a nota' ]) }}

                    {{ Form::label('label', 'Relativas ao conteúdo', ['class' => 'font-weight-bold']) }}
                    {{ Form::text('area7[]', '', ['class' => 'form-control mb-3', 'placeholder' => 'Escreva aqui a nota' ]) }}

                    {{ Form::label('label', 'Identificador de recursos e termos da área de disponibilidade', ['class' => 'font-weight-bold']) }}
                    {{ Form::text('area7[]', '', ['class' => 'form-control mb-3', 'placeholder' => 'Escreva aqui a nota' ]) }}

                    {{ Form::label('label', 'Questão, parte, iteração, etc., que formam a base da descrição', ['class' => 'font-weight-bold']) }}
                    {{ Form::text('area7[]', '', ['class' => 'form-control mb-3', 'placeholder' => 'Escreva aqui a nota' ]) }}

                    {{ Form::label('label', 'Outras anotações', ['class' => 'font-weight-bold']) }}
                    {{ Form::text('area7[]', '', ['class' => 'form-control mb-3', 'placeholder' => 'Escreva aqui a nota' ]) }}

                    {{ Form::label('label', 'Relativas à cópia em mãos', ['class' => 'font-weight-bold']) }}
                    {{ Form::text('area7[]', '', ['class' => 'form-control mb-3', 'placeholder' => 'Escreva aqui a nota' ]) }}


                </div>
            </div>

            <!-- AREA 8 -->
            <p class="col-md-10">
                <button class="btn btn-outline-dark btn-sm" type="button" data-toggle="collapse" data-target="#area-8" aria-expanded="false" aria-controls="collapseExample">
                    Mostrar | Esconder
                </button>
                &nbsp; <span class="font-weight-bold"> Área 8: </span>
                <span class="text-muted"> Identificador de recursos e te rms da área de disponibilidade </span>
            </p>

            <div class="collapse" id="area-8">
                <div class="card card-body">

                    {{ Form::label('label', 'Identificador de Recurso', ['class' => 'font-weight-bold']) }}
                    {{ Form::text('area8[]', '', ['class' => 'form-control mb-3', 'placeholder' => 'Insira aqui o número normalizado (ISBN ...)' ]) }}

                    {{ Form::label('label', 'Título da chave (recursos contínuos)', ['class' => 'font-weight-bold']) }}
                    {{ Form::text('area8[]', '', ['class' => 'form-control mb-3', 'placeholder' => 'Título da chave (recursos contínuos)' ]) }}

                    {{ Form::label('label', 'Termos de disponibilidade', ['class' => 'font-weight-bold']) }}
                    {{ Form::text('area8[]', '', ['class' => 'form-control mb-3', 'placeholder' => 'Termos de disponibilidade' ]) }}

                </div>
            </div>


        {{ Form::submit('Concluir', ['class' => 'btn btn-primary mb-5']) }}
        {!! Form::close() !!}

    </div>

@endsection