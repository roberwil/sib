@extends('layouts.app')
@section('page-description', 'Ver Exemplares')
@section('content')

    {!! \App\Helpers\Sib::buildGoBackBtn('/catalog/view/material') !!}

    <div class="col-md-12">

        <table class="table">
            <thead class="thead-dark">
            <tr>
                <th scope="col"> COD  </th>
                <th scope="col"> Nº </th>
                <th scope="col"> Nº de Chamada</th>
                <th scope="col"> Etiqueta </th>
                <th scope="col"> Disponível </th>
                <th class="text-center" scope="col"> Localização </th>
                <th scope="col"> Opções </th>
            </tr>
            </thead>

            <tbody>

            @if(count($exemplaries) === 0)
                <tr class="alert alert-dark">
                    <td class="text-center font-weight-bold" colspan="6"> Sem itens </td>
                </tr>
            @endif

            @foreach($exemplaries as $exemplary)
                <tr>
                    <td> {{ $exemplary->id  }} </td>
                    <td> {{ $exemplary->number }} </td>
                    <td> {{ $exemplary->callNumber }} </td>
                    <td> {{ \App\Helpers\Sib::resolveLabel($exemplary->label) }} </td>
                    <td> {{ $exemplary->available ? 'Sim' : 'Não' }} </td>

                    <td class="text-center">
                        <button class="btn btn-outline-dark btn-sm"
                           data-container="body"
                           data-toggle="popover"
                           data-placement="bottom"
                           data-content="Localização">
                            <i class="fa fa-eye"></i>
                        </button>
                    </td>

                    <td class="dropdown">
                        <div class="row">
                            <div class="col-md-10">
                                <button class="btn btn-outline-secondary btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Opções</button>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="" data-toggle="modal" data-target="#edit-exemplary-{{$exemplary->id}}"> <i class="fa fa-edit"></i> &nbsp; Editar </a>
                                    <a class="dropdown-item" href="/catalog/delete/3/{{$exemplary->id}}"><i class="fa fa-remove"></i> &nbsp; Eliminar </a>
                                </div>
                            </div>
                        </div>
                    </td>

                </tr>
            @endforeach
            </tbody>
        </table>

    </div>

@endsection
