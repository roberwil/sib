@extends('layouts.app')
@section('page-description', 'Emprestar Material')
@section('content')

    <div class="col-md-4">

        <p id="book-unavailable" class="alert alert-danger"> Material Indisponivel </p>
        <p id="book-available" class="alert alert-success"> Material Disponível </p>
        <input id="bb-book" type="text" class="form-control" placeholder="Insira aqui o código do livro">
        <buttom id="bb-book-btn" href="" class="btn btn-primary mt-3">Verificar disponibilidade</buttom> <br>

        <p id="user-unavailable" class="alert alert-danger"> Utilizador não pode ter empréstimos </p>
        <p id="user-available" class="alert alert-success"> Utilizador pode efectuar empréstimos </p>
        <input id="bb-user" type="text" class="form-control mt-3" placeholder="Insira aqui o nome de usuário a requisitar o empréstimo">
        <button id="bb-user-btn" href="" class="btn btn-primary mt-3">Verificar disponibilidade</button> <br>

    </div>

@endsection