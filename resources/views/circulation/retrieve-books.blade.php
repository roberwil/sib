@extends('layouts.app')
@section('page-description', 'Devolver Material')
@section('content')

    <div class="col-md-4">

        {!! Form::open(['url' => '/circulation/retrieve']) !!}

            {{ Form::label('code', 'Código do Material Emprestado', ['class' => 'font-weight-bold'])  }}
            {{ Form::text('code', '', ['class' => 'form-control mb-2', 'required', 'placeholder' => 'Digite aqui o código do material']) }}

            {{ Form::submit('Devolver', ['class' => 'btn btn-primary']) }}

        {!! Form::close() !!}
    </div>

@endsection