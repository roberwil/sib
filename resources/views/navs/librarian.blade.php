
<ul id="main-menu" class="main-menu collapse navbar-collapse">
    <ul class="nav navbar-nav">

        <li class="active">
            <a href="/catalog/search"> <i class="menu-icon fa fa-search"></i> Pesquisa </a>
        </li>

        <h3 class="menu-title">Módulos</h3><!-- /.menu-title -->

        @if(\App\Helpers\Sib::getAttribute('bibliotecarios', 'level', session('user')) != 3)
            <li class="menu-item-has-children dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-book"></i>Catalogação</a>
                <ul class="sub-menu children dropdown-menu">
                    <li><i class="fa fa-eye"></i><a href="/catalog/view/material"> Ver Materiais </a></li>
                    <li><i class="fa fa-plus-square"></i><a href="/catalog/register/new/view/1">Registar Novo</a></li>
                    <li><i class="fa fa-plus-square"></i><a href="" data-toggle="modal" data-target="#register-exemplary">Adicionar Exemplar</a></li>
                </ul>
            </li>
        @endif

        <li class="menu-item-has-children dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-retweet"></i>Circulação</a>
            <ul class="sub-menu children dropdown-menu">
                <li><i class="fa fa-fast-forward"></i><a href="/circulation/view-borrow">Emprestar Livros</a></li>
                <li><i class="fa fa-fast-backward"></i><a href="/circulation/view-deliver">Devolver Livros</a></li>
            </ul>
        </li>

        @if(\App\Helpers\Sib::getAttribute('bibliotecarios', 'level', session('user')) == 1)
            <li class="menu-item-has-children dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-bar-chart"></i>Estatísticas</a>
                <ul class="sub-menu children dropdown-menu">
                    <li><i class="menu-icon fa fa-eye"></i><a href="/stats/view">Ver Estatísticas</a></li>
                </ul>
            </li>
        @endif

        @if(\App\Helpers\Sib::getAttribute('bibliotecarios', 'level', session('user')) != 3)
            <h3 class="menu-title">Configuração</h3><!-- /.menu-title -->
            <li class="menu-item-has-children dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-users"></i>Utilizadores e Grupos</a>
                <ul class="sub-menu children dropdown-menu">

                    @if(\App\Helpers\Sib::getAttribute('bibliotecarios', 'level', session('user')) == 1)
                    <li><a href="/users/view/2"> <i class="fa fa-eye"></i> Ver Bibliotecários</a></li>
                    <li><a href="" data-toggle="modal" data-target="#register-librarian"> <i class="fa fa-user-plus"></i> Registrar Bibliotecário</a></li>
                    @endif

                    <li><a href="/users/view/3"> <i class="fa fa-eye"></i> Ver Leitores</a></li>
                    <li><a href="" data-toggle="modal" data-target="#register-userbib"> <i class="fa fa-user-plus"></i> Registrar Leitor</a></li>

                    @if(\App\Helpers\Sib::getAttribute('bibliotecarios', 'level', session('user')) == 1)
                    <li><a href="/groups/view"> <i class="fa fa-eye"></i> Ver Grupos</a></li>
                    <li><a href="" data-toggle="modal" data-target="#register-group"> <i class="fa fa-user-plus"></i> Registrar Grupo</a></li>
                    @endif

                </ul>
            </li>

            @if(\App\Helpers\Sib::getAttribute('bibliotecarios', 'level', session('user')) == 1)
                <li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-tasks"></i>Biblioteca</a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><a href="" data-toggle="modal" data-target="#config-edit-library"> <i class="fa fa-cog"></i> Configurar/Editar</a></li>
                        <li><a href="/library/view"> <i class="fa fa-eye"></i>Ver Configurações</a></li>
                        @if(!\App\Biblioteca::getInfo()['configMode'])
                            <li><a href="" data-toggle="modal" data-target="#register-floor"> <i class="fa fa-plus-square"></i> Adicionar Andar</a></li>
                            <li><a href="/library/bookcases/view"> <i class="fa fa-eye"></i> Ver Estantes</a></li>
                            <li><a href="" data-toggle="modal" data-target="#register-bookcase"> <i class="fa fa-plus-square"></i> Adicionar Estante</a></li>
                        @endif
                    </ul>
                </li>
            @endif
        @endif


    </ul>

</ul><!-- /.navbar-collapse -->