<ul id="main-menu" class="main-menu collapse navbar-collapse">
    <ul class="nav navbar-nav">

        <li class="active">
            <a href="/catalog/search"> <i class="menu-icon fa fa-search"></i> Pesquisa </a>
        </li>

        <h3 class="menu-title">Configuração</h3><!-- /.menu-title -->

        <li class="menu-item-has-children dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-users"></i>Utilizadores e Grupos</a>
            <ul class="sub-menu children dropdown-menu">

                <li><a href="/users/view/1"> <i class="fa fa-eye"></i> Ver Administradores</a></li>
                <li><a href="" data-toggle="modal" data-target="#register-admin"> <i class="fa fa-plus-square"></i> Registrar Administrador</a></li>

                <li><a href="/users/view/2"> <i class="fa fa-eye"></i> Ver Bibliotecários</a></li>
                <li><a href="" data-toggle="modal" data-target="#register-librarian"> <i class="fa fa-plus-square"></i> Registrar Bibliotecário</a></li>

                <li><a href="/users/view/3"> <i class="fa fa-eye"></i> Ver Leitores</a></li>
                <li><a href="" data-toggle="modal" data-target="#register-userbib"> <i class="fa fa-plus-square"></i> Registrar Leitor</a></li>

                <li><a href="/groups/view"> <i class="fa fa-eye"></i> Ver Grupos</a></li>
                <li><a href="" data-toggle="modal" data-target="#register-group"> <i class="fa fa-plus-square"></i> Registrar Grupo</a></li>

            </ul>
        </li>

    </ul>

</ul><!-- /.navbar-collapse -->