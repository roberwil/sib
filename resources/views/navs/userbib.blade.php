<ul id="main-menu" class="main-menu collapse navbar-collapse">
    <ul class="nav navbar-nav">

        <li class="active">
            <a href="/catalog/search"> <i class="menu-icon fa fa-search"></i> Pesquisa </a>
        </li>

        <h3 class="menu-title">Módulos</h3><!-- /.menu-title -->

        <li class="">
            <a href="#"> <i class="menu-icon fa fa-book"></i>Meus empréstimos</a>
        </li>

        <li class="menu-item-has-children dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-database"></i>Reservas</a>
            <ul class="sub-menu children dropdown-menu">
                <li><i class="fa fa-play-circle"></i><a href="/catalog/reservations/make">Fazer reserva</a></li>
                <li><i class="fa fa-eye"></i><a href="/catalog/reservations/view">Minhas Reservas</a></li>
            </ul>
        </li>

    </ul>

</ul><!-- /.navbar-collapse -->