@extends('layouts.app')

@section('page-description', 'Gerir Leitores')
@section('content')

    <div class="col-md-12">

        <table class="table">
            <thead class="thead-dark">
            <tr>
                <th scope="col"> Nome </th>
                <th scope="col"> Username </th>
                <th scope="col"> Email </th>
                <th scope="col"> Grupo </th>
                <th scope="col"> Editar </th>
                <th scope="col"> Eliminar </th>
            </tr>
            </thead>

            <tbody>

            @if(count($readers) === 0)
                <tr class="alert alert-dark">
                    <td class="text-center font-weight-bold" colspan="6"> Sem itens </td>
                </tr>
            @endif

            @foreach($readers as $reader)
                <tr>
                    <td> {{  $reader->fullname }} </td>
                    <td> {{  $reader->username }} </td>
                    <td> {{  $reader->email }} </td>
                    <td> {{  \App\Grupo::retriveDescriptionById($reader->groupId) }} </td>
                    <td> <a href="" data-toggle="modal" data-target="#edit-userbib-{{ $reader->id }}"> <i class="fa fa-edit"></i></a> </td>
                    <td> <a href="/users/delete/3/{{\Illuminate\Support\Facades\Crypt::encrypt( $reader->id )}}"><i class="fa fa-remove"></i></a> </td>
                </tr>
            @endforeach
            </tbody>
        </table>

    </div>

@endsection

