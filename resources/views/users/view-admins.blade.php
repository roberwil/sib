@extends('layouts.app')

@section('page-description', 'Gerir Administradores')
@section('content')

    <div class="col-md-12">

        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <th scope="col"> Nome </th>
                    <th scope="col"> Username </th>
                    <th scope="col"> Email </th>
                    <th scope="col"> Editar </th>
                    <th scope="col"> Eliminar </th>
                </tr>
            </thead>

            <tbody>

                @if(count($admins) === 0)
                    <tr class="alert alert-dark">
                        <td class="text-center font-weight-bold" colspan="6"> Sem itens </td>
                    </tr>
                @endif

                @foreach($admins as $admin)
                    <tr>
                        <td> {{  $admin->fullname }} </td>
                        <td> {{  $admin->username }} </td>
                        <td> {{  $admin->email }} </td>
                        <td> <a href="" data-toggle="modal" data-target="#edit-admin-{{ $admin->id }}"> <i class="fa fa-edit"></i></a> </td>
                        <td> <a href="/users/delete/1/{{\Illuminate\Support\Facades\Crypt::encrypt( $admin->id )}}"><i class="fa fa-remove"></i></a> </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

    </div>

@endsection

