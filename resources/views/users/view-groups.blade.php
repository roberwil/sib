@extends('layouts.app')
@section('page-description', 'Gerir Grupos')
@section('content')

    <div class="col-md-12">

        <table class="table">
            <thead class="thead-dark">
            <tr>
                <th scope="col"> Descrição </th>
                <th> Mais Detalhes </th>
                <th scope="col"> Editar </th>
                <th scope="col"> Eliminar </th>
            </tr>
            </thead>

            <tbody>

            @if(count($groups) === 0)
                <tr class="alert alert-dark">
                    <td class="text-center font-weight-bold" colspan="6"> Sem itens </td>
                </tr>
            @endif

            @foreach($groups as $group)
                <tr>
                    <td> {{  $group->description }} </td>
                    <td> <a href="" data-toggle="modal" data-target="#view-group-detail-{{ $group->id }}"> <i class="fa fa-eye"></i></a> </td>
                    <td> <a href="" data-toggle="modal" data-target="#edit-group-{{ $group->id }}"> <i class="fa fa-edit"></i></a> </td>
                    <td> <a href="/groups/delete/{{\Illuminate\Support\Facades\Crypt::encrypt( $group->id )}}"><i class="fa fa-remove"></i></a> </td>
                </tr>
            @endforeach
            </tbody>
        </table>

    </div>

@endsection