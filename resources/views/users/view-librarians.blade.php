@extends('layouts.app')

@section('page-description', 'Gerir Bibiotecários')
@section('content')

    <div class="col-md-12">

        <table class="table">
            <thead class="thead-dark">
            <tr>
                <th scope="col"> Nome </th>
                <th scope="col"> Username </th>
                <th scope="col"> Email </th>
                <th scope="col"> Nível de Acesso </th>
                <th scope="col"> Editar </th>
                <th scope="col"> Eliminar </th>
            </tr>
            </thead>

            <tbody>

            @if(count($librarians) === 0)
                <tr class="alert alert-dark">
                    <td class="text-center font-weight-bold" colspan="6"> Sem itens </td>
                </tr>
            @endif

            @foreach($librarians as $librarian)
                <tr>
                    <td> {{  $librarian->fullname }} </td>
                    <td> {{  $librarian->username }} </td>
                    <td> {{  $librarian->email }} </td>
                    <td> {{  $librarian->level }} </td>
                    <td> <a href="" data-toggle="modal" data-target="#edit-librarian-{{ $librarian->id }}"> <i class="fa fa-edit"></i></a> </td>
                    <td> <a href="/users/delete/2/{{\Illuminate\Support\Facades\Crypt::encrypt( $librarian->id )}}"><i class="fa fa-remove"></i></a> </td>
                </tr>
            @endforeach
            </tbody>
        </table>

    </div>

@endsection