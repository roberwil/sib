

<div class="modal fade" id="register-exemplary" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <!-- MODAL TITLE GOES IN THE FOLLOWING SECTION -->
                <h5 class="modal-title" id="exampleModalLongTitle">
                    Adicionar Exemplar
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <!-- MAIN MODAL CONTENT GOES IN THE FOLLOWING SECTION -->
            {!! Form::open(['url' => '/catalog/register/exemplary']) !!}
                <div class="modal-body">
                    <div class="form-control">

                        {{ Form::label('material', 'Material', ['class' => 'font-weight-bold'])  }}
                        {{ Form::select('material', \App\Material::get(), '', ['class' => 'form-control', 'required']) }}

                        {{ Form::label('label', 'Etiqueta', ['class' => 'font-weight-bold'])  }}
                        {{ Form::select('label', [ '1' => 'Vermelha', '2' => 'Amarela', '3' => 'Branca' ], 1, ['class' => 'form-control', 'required']) }}

                    </div>
                </div>
        <!-- FOOTER BUTTONS GO IN THE FOLLOWING SECTION-->
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"> Cancelar </button>
                {{ Form::submit('Adicionar', ['class' => 'btn btn-primary']) }}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
