@if (isset($materials))
    @foreach($materials as $material)
        <div class="modal fade" id="view-material-detail-{{ $material->id }}" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <!-- MODAL TITLE GOES IN THE FOLLOWING SECTION -->
                        <h5 class="modal-title" id="exampleModalLongTitle">
                            Ver Detalhes
                        </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <!-- MAIN MODAL CONTENT GOES IN THE FOLLOWING SECTION -->
                    <div class="modal-body">
                        <table class="table">

                            <tbody>

                            <tr>
                                <th scope="col"> Tipo </th>
                                <td> {{ \App\Helpers\Sib::resolveMaterialType($material->type)  }} </td>
                            </tr>

                            <tr>
                                <th scope="col"> Classificação  </th>
                                <td> {{  $material->classification }} </td>
                            </tr>

                            <tr>
                                <th scope="col"> Número de Exemplares </th>
                                <td> {{  $material->nExemplary }} </td>
                            </tr>

                            <tr>
                                <th scope="col"> Edição </th>
                                <td> {{$material->edition }} </td>
                            </tr>

                            <tr>
                                <th scope="col"> Ano de Edição </th>
                                <td> {{  $material->editionYear }} </td>
                            </tr>

                            <tr>
                                <th scope="col"> Editora </th>
                                <td> {{  $material->publisher }} </td>
                            </tr>

                            <tr>
                                <th scope="col"> Ano de Publicação </th>
                                <td> {{  $material->publishingYear }} </td>
                            </tr>

                            </tbody>

                        </table>
                    </div>
                    <!-- FOOTER BUTTONS GO IN THE FOLLOWING SECTION-->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal"> Fechar </button>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endif