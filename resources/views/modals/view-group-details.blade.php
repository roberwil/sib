@if (isset($groups))
@foreach($groups as $group)
<div class="modal fade" id="view-group-detail-{{ $group->id }}" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <!-- MODAL TITLE GOES IN THE FOLLOWING SECTION -->
                <h5 class="modal-title" id="exampleModalLongTitle">
                    Ver Detalhes
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <!-- MAIN MODAL CONTENT GOES IN THE FOLLOWING SECTION -->
            <div class="modal-body">
                <table class="table">

                    <tbody>

                        <tr>
                            <th scope="col"> Número máximo de Empéstimos </th>
                            <td> {{  $group->maxNumberBorrow }} </td>
                        </tr>

                        <tr>
                            <th scope="col"> Multa de Empréstimo de Livros  </th>
                            <td> {{  $group->fineBooks }} </td>
                        </tr>

                        <tr>
                            <th scope="col"> Tempo Máximo de Empréstimo (em dias) </th>
                            <td> {{  $group->maxTimeBorrow }} </td>
                        </tr>

                        <tr>
                            <th scope="col"> Tempo Mínimo de Empréstimo (em dias) </th>
                            <td> {{  $group->minTimeBorrow }} </td>
                        </tr>

                        <tr>
                            <th scope="col"> Número Máximo de Reservas </th>
                            <td> {{  $group->maxReservations }} </td>
                        </tr>

                        <tr>
                            <th scope="col"> Número Máximo de Renovações </th>
                            <td> {{  $group->minTimeBorrow }} </td>
                        </tr>

                    </tbody>

                </table>
            </div>
            <!-- FOOTER BUTTONS GO IN THE FOLLOWING SECTION-->
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"> Fechar </button>
            </div>
        </div>
    </div>
</div>
@endforeach
@endif