@if(isset($librarians))
    @foreach($librarians as $librarian)
        <div class="modal fade" id="edit-librarian-{{$librarian->id}}" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <!-- MODAL TITLE GOES IN THE FOLLOWING SECTION -->
                        <h5 class="modal-title" id="">
                            Editar Bibliotecário
                        </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <!-- MAIN MODAL CONTENT GOES IN THE FOLLOWING SECTION -->
                    {!! Form::open(['url' => '/users/register/librarian/' . $librarian->id ]) !!}
                    <div class="modal-body">
                        <div class="form-control">

                            {{ Form::label('fullname', 'Nome Completo', ['class' => 'font-weight-bold'])  }}
                            {{ Form::text('fullname', $librarian->fullname, ['class' => 'form-control', 'placeholder' => 'Insira aqui o nome completo do bibliotecário']) }}

                            {{ Form::label('email', 'E-mail', ['class' => 'font-weight-bold'])  }}
                            {{ Form::email('email', $librarian->email, ['class' => 'form-control', 'placeholder' => 'Insira aqui o e-mail do bibliotecário']) }}

                            {{ Form::label('username', 'Nome de Utilizador', ['class' => 'font-weight-bold'])  }}
                            {{ Form::text('username', $librarian->username, ['class' => 'form-control', 'placeholder' => 'O nome de utilizador não deve conter espaços']) }}

                            {{ Form::label('level', 'Nível de Acesso', ['class' => 'font-weight-bold'])  }}
                            {{ Form::selectRange('level', 1, 3, $librarian->level, ['class' => 'form-control']) }}

                        </div>
                    </div>
                    <!-- FOOTER BUTTONS GO IN THE FOLLOWING SECTION-->
                    <div class="modal-footer">
                        <a href="/users/view/2" class="btn btn-secondary" data-dismiss="modal"> Cancelar </a>
                        {{ Form::submit('Guardar', ['class' => 'btn btn-primary']) }}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    @endforeach
@endif