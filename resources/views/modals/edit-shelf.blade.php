@if(isset($shelves))
    @foreach($shelves as $shelf)

        <div class="modal fade" id="edit-shelf-{{ $shelf->id }}" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <!-- MODAL TITLE GOES IN THE FOLLOWING SECTION -->
                        <h5 class="modal-title" id="exampleModalLongTitle">
                            Editar Prateleira
                        </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <!-- MAIN MODAL CONTENT GOES IN THE FOLLOWING SECTION -->
                    {!! Form::open(['url' => '/library/bookcases/shelves/register/' . $shelf->id]) !!}
                        <div class="modal-body">
                            <div class="form-control">

                                <label for="cdd" class="font-weight-bold">
                                    CDD Nível 2: &nbsp; <span class="text-primary"> {{ \App\Helpers\Sib::resolveCdd('cdd-level-2', $shelf->cdd, 1) }}</span>
                                </label>
                                {{ Form::select('cdd', \App\Helpers\Sib::getJsonData('cdd-level-2'), $shelf->cdd, ['class' => 'form-control', 'required']) }}

                                {{ Form::label('maxBooks', 'Número de Livros', ['class' => 'font-weight-bold'])  }}
                                {{ Form::number('maxBooks', $shelf->maxBooks, ['class' => 'form-control', 'required']) }}

                                {{ Form::label('bookcase', 'Estante', ['class' => 'invisible'])  }}
                                {{ Form::text('bookcase', $shelf->bookcase, ['class' => 'invisible', 'required']) }}

                            </div>
                        </div>
                        <!-- FOOTER BUTTONS GO IN THE FOLLOWING SECTION-->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal"> Cancelar </button>
                            {{ Form::submit('Guardar', ['class' => 'btn btn-primary']) }}
                        </div>
                    {!! Form::close() !!}

                </div>
            </div>
        </div>

    @endforeach
@endif