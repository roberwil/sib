@if(isset($floors))
@foreach($floors as $floor)
<div class="modal fade" id="edit-floor-{{ $floor->id }}" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <!-- MODAL TITLE GOES IN THE FOLLOWING SECTION -->
                <h5 class="modal-title" id="exampleModalLongTitle">
                    Editar andar
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <!-- MAIN MODAL CONTENT GOES IN THE FOLLOWING SECTION -->
            {!! Form::open(['url' => '/library/floor/register/' . $floor->id ]) !!}
                <div class="modal-body">
                    <div class="form-control">

                        {{ Form::label('description', 'Descrição', ['class' => 'font-weight-bold'])  }}
                        {{ Form::text('description', $floor->description, ['class' => 'form-control', 'readonly' ]) }}

                        {{ Form::label('nLines', 'Número de Filas', ['class' => 'font-weight-bold'])  }}
                        {{ Form::number('nLines', $floor->nLines, ['class' => 'form-control', 'required']) }}

                    </div>
                </div>
                <!-- FOOTER BUTTONS GO IN THE FOLLOWING SECTION-->
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"> Cancelar </button>
                    {{ Form::submit('Guardar', ['class' => 'btn btn-primary']) }}
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endforeach
@endif