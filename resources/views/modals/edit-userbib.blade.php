@if (isset($reader))
@foreach ($readers as $reader)
<div class="modal fade" id="edit-userbib-{{$reader->id}}" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <!-- MODAL TITLE GOES IN THE FOLLOWING SECTION -->
                <h5 class="modal-title" id="">
                    Editar Leitor
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <!-- MAIN MODAL CONTENT GOES IN THE FOLLOWING SECTION -->
            {!! Form::open(['url' => '/users/register/userbib/' . $reader->id ]) !!}
            <div class="modal-body">
                <div class="form-control">

                    {{ Form::label('fullname', 'Nome Completo', ['class' => 'font-weight-bold'])  }}
                    {{ Form::text('fullname', $reader->fullname, ['class' => 'form-control', 'placeholder' => 'Insira aqui o nome completo do bibliotecário', 'required']) }}

                    {{ Form::label('email', 'E-mail', ['class' => 'font-weight-bold'])  }}
                    {{ Form::email('email', $reader->email, ['class' => 'form-control', 'placeholder' => 'Insira aqui o e-mail do bibliotecário', 'required']) }}

                    {{ Form::label('username', 'Nome de Utilizador', ['class' => 'font-weight-bold'])  }}
                    {{ Form::text('username', $reader->username, ['class' => 'form-control', 'placeholder' => 'O nome de utilizador não deve conter espaços', 'required']) }}

                    {{ Form::label('groupId', 'Grupo', ['class' => 'font-weight-bold'])  }}
                    {{ Form::select('groupId', \App\Grupo::retrieveGroupsArray(), $reader->groupId, ['class' => 'form-control', 'required']) }}

                </div>
            </div>
            <!-- FOOTER BUTTONS GO IN THE FOLLOWING SECTION-->
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"> Cancelar </button>
                {{ Form::submit('Guardar', ['class' => 'btn btn-primary']) }}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endforeach
@endif