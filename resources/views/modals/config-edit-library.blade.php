
@if (\App\Biblioteca::getInfo()['configMode'])
    <div class="modal fade" id="config-edit-library" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <!-- MODAL TITLE GOES IN THE FOLLOWING SECTION -->
                    <h5 class="modal-title" id="exampleModalLongTitle">
                        Configurar Biblioteca
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <!-- MAIN MODAL CONTENT GOES IN THE FOLLOWING SECTION -->
                {!! Form::open(['url' => '/library/config']) !!}
                <div class="modal-body">
                    <div class="form-control">

                        {{ Form::label('name', 'Nome', ['class' => 'font-weight-bold'])  }}
                        {{ Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'Insira aqui o nome da Biblioteca', 'required']) }}

                        {{ Form::label('prefix', 'Prefixo', ['class' => 'font-weight-bold'])  }}
                        {{ Form::text('prefix', '', ['class' => 'form-control', 'placeholder' => 'Insira aqui o prefixo da biblioteca', 'required']) }}

                        {{ Form::label('nFloors', 'Número de Andares', ['class' => 'font-weight-bold'])  }}
                        {{ Form::number('nFloors', 0, ['class' => 'form-control', 'required']) }}

                    </div>
                </div>
                <!-- FOOTER BUTTONS GO IN THE FOLLOWING SECTION-->
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"> Cancelar </button>
                    {{ Form::submit('Salvar Configurações', ['class' => 'btn btn-primary']) }}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@else
    <div class="modal fade" id="config-edit-library" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <!-- MODAL TITLE GOES IN THE FOLLOWING SECTION -->
                    <h5 class="modal-title" id="exampleModalLongTitle">
                        Editar Configurações da Biblioteca
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <!-- MAIN MODAL CONTENT GOES IN THE FOLLOWING SECTION -->
                {!! Form::open(['url' => '/library/config/' . \App\Biblioteca::getInfo()['data']['id']]) !!}
                <div class="modal-body">
                    <div class="form-control">

                        {{ Form::label('name', 'Nome', ['class' => 'font-weight-bold'])  }}
                        {{ Form::text('name', \App\Biblioteca::getInfo()['data']['name'], ['class' => 'form-control', 'placeholder' => 'Insira aqui o nome da Biblioteca', 'required']) }}

                        {{ Form::label('prefix', 'Prefixo', ['class' => 'font-weight-bold'])  }}
                        {{ Form::text('prefix', \App\Biblioteca::getInfo()['data']['prefix'], ['class' => 'form-control', 'placeholder' => 'Insira aqui o prefixo da biblioteca', 'required']) }}

                        {{ Form::label('nFloors', 'Número de Andares', ['class' => 'font-weight-bold'])  }}
                        {{ Form::number('nFloors', \App\Biblioteca::getInfo()['data']['nFloors'], ['class' => 'form-control', 'required']) }}

                    </div>
                </div>
                <!-- FOOTER BUTTONS GO IN THE FOLLOWING SECTION-->
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"> Cancelar </button>
                    {{ Form::submit('Salvar Configurações', ['class' => 'btn btn-primary']) }}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endif

