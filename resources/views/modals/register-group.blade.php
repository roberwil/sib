
<div class="modal fade" id="register-group" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <!-- MODAL TITLE GOES IN THE FOLLOWING SECTION -->
                <h5 class="modal-title" id="exampleModalLongTitle">
                    Registrar novo Grupo
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <!-- MAIN MODAL CONTENT GOES IN THE FOLLOWING SECTION -->
            {!! Form::open(['url' => '/groups/register']) !!}
            <div class="modal-body">
                <div class="form-control">

                    {{ Form::label('description', 'Descrição', ['class' => 'font-weight-bold'])  }}
                    {{ Form::text('description', '', ['class' => 'form-control', 'placeholder' => 'Insira aqui o uma descrição para o grupo', 'required']) }}

                    {{ Form::label('maxNumberBorrow', 'Número Máximo de Empéstimos', ['class' => 'font-weight-bold'])  }}
                    {{ Form::selectRange('maxNumberBorrow', 1, 10, '', ['class' => 'form-control', 'required']) }}

                    {{ Form::label('fineBooks', 'Multa de Empréstimo de Livros (por dia)', ['class' => 'font-weight-bold'])  }}
                    {{ Form::number('fineBooks', 0, ['class' => 'form-control', 'required']) }}

                    {{ Form::label('maxTimeBorrow', 'Tempo Máximo de Empréstimo (em dias)', ['class' => 'font-weight-bold'])  }}
                    {{ Form::number('maxTimeBorrow', 0, ['class' => 'form-control', 'required']) }}

                    {{ Form::label('minTimeBorrow', 'Tempo Mínimo de Empréstimo (em dias)', ['class' => 'font-weight-bold'])  }}
                    {{ Form::number('minTimeBorrow', 0, ['class' => 'form-control', 'required']) }}

                    {{ Form::label('maxReservations', 'Número máximo de Reservas', ['class' => 'font-weight-bold'])  }}
                    {{ Form::number('maxReservations', 0, ['class' => 'form-control', 'required']) }}

                    {{ Form::label('maxTimeReservations', 'Tempo limite da Reserva (em dias)', ['class' => 'font-weight-bold'])  }}
                    {{ Form::number('maxTimeReservations', 0, ['class' => 'form-control', 'required']) }}

                    {{ Form::label('maxRenewals', 'Número máximo de renovações (por empréstimo)', ['class' => 'font-weight-bold'])  }}
                    {{ Form::number('maxRenewals', 0, ['class' => 'form-control', 'required']) }}



                </div>
            </div>
            <!-- FOOTER BUTTONS GO IN THE FOLLOWING SECTION-->
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"> Cancelar </button>
                {{ Form::submit('Registrar', ['class' => 'btn btn-primary']) }}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>