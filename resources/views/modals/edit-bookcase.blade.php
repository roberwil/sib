@if (isset($bookcases))
    @foreach($bookcases as $bookcase)
        <div class="modal fade" id="edit-bookcase-{{$bookcase->id}}" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <!-- MODAL TITLE GOES IN THE FOLLOWING SECTION -->
                        <h5 class="modal-title" id="exampleModalLongTitle">
                            Editar Estante
                        </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <!-- MAIN MODAL CONTENT GOES IN THE FOLLOWING SECTION -->
                    {!! Form::open(['url' => '/library/bookcases/register/' . $bookcase->id]) !!}
                    <div class="modal-body">
                        <div class="form-control">

                            {{ Form::label('description', 'Descrição', ['class' => 'font-weight-bold'])  }}
                            {{ Form::text('description', $bookcase->description, ['class' => 'form-control', 'readonly']) }}

                            <label for="cdd" class="font-weight-bold">
                                CDD Nível 1: &nbsp; <span class="text-primary"> {{  \App\Helpers\Sib::resolveCdd('cdd-level-1', $bookcase->cdd, 1) }}</span>
                            </label>
                            {{ Form::select('cdd', \App\Helpers\Sib::getJsonData('cdd-level-1'), $bookcase->cdd, ['class' => 'form-control', 'required']) }}

                            {{ Form::label('nShelves', 'Número de Prateleiras', ['class' => 'font-weight-bold'])  }}
                            {{ Form::number('nShelves', $bookcase->nShelves, ['class' => 'form-control', 'required']) }}

                            <p class="alert alert-primary mt-3">
                                Localização da Estante
                            </p>

                            <label for="floor" class="font-weight-bold">
                                Andar: &nbsp; <span class="text-primary"> {{ \App\Andar::getDescriptionById($bookcase->floor) }}</span>
                            </label>
                            {{ Form::select('floor', \App\Biblioteca::getFloors(), $bookcase->floor, ['class' => 'form-control eb-change-lines', 'required']) }}


                            <label for="line" class="font-weight-bold">
                                Fila nº: &nbsp; <span class="text-primary"> {{ $bookcase->line }}</span>
                            </label>
                            <select name="line" class="form-control eb-lines" required>
                                <option value="{{ $bookcase->line }}"> Selecione a fila </option>
                            </select>

                        </div>
                    </div>
                    <!-- FOOTER BUTTONS GO IN THE FOLLOWING SECTION-->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal"> Cancelar </button>
                        {{ Form::submit('Guardar', ['class' => 'btn btn-primary']) }}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    @endforeach
@endif
