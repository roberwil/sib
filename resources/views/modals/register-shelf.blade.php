@if(isset($bookcases))
    @foreach($bookcases as $bookcase)

        <div class="modal fade" id="register-shelf-{{ $bookcase->id }}" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <!-- MODAL TITLE GOES IN THE FOLLOWING SECTION -->
                        <h5 class="modal-title" id="exampleModalLongTitle">
                            Adicionar Prateleira
                        </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @if (\App\Prateleira::blockRegister($bookcase->id))
                    <div class="alert alert-danger">
                        <p class="font-weight-bold"> Atingiu o limite máximo </p>
                    </div>
                    @else
                    <!-- MAIN MODAL CONTENT GOES IN THE FOLLOWING SECTION -->
                    {!! Form::open(['url' => '/library/bookcases/shelves/register']) !!}
                    <div class="modal-body">
                        <div class="form-control">

                            {{ Form::label('cdd', 'CDD Nível 2', ['class' => 'font-weight-bold'])  }}
                            {{ Form::select('cdd', \App\Helpers\Sib::getJsonData('cdd-level-2'), '', ['class' => 'form-control', 'required']) }}

                            {{ Form::label('maxBooks', 'Número de Livros', ['class' => 'font-weight-bold'])  }}
                            {{ Form::number('maxBooks', 0, ['class' => 'form-control', 'required']) }}

                            {{ Form::label('bookcase', 'Estante', ['class' => 'invisible'])  }}
                            {{ Form::text('bookcase', $bookcase->id, ['class' => 'invisible', 'required']) }}

                        </div>
                    </div>
                    <!-- FOOTER BUTTONS GO IN THE FOLLOWING SECTION-->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal"> Cancelar </button>
                        {{ Form::submit('Registrar', ['class' => 'btn btn-primary']) }}
                    </div>
                    {!! Form::close() !!}
                    @endif
                </div>
            </div>
        </div>

    @endforeach
@endif