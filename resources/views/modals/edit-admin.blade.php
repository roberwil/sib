@if(isset($admins))
@foreach($admins as $admin)
<div class="modal fade" id="edit-admin-{{$admin->id}}" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <!-- MODAL TITLE GOES IN THE FOLLOWING SECTION -->
                <h5 class="modal-title" id="">
                    Editar Administrador
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <!-- MAIN MODAL CONTENT GOES IN THE FOLLOWING SECTION -->
            {!! Form::open(['url' => '/users/register/admin/' . $admin->id ]) !!}
            <div class="modal-body">
                <div class="form-control">

                    {{ Form::label('fullname', 'Nome Completo', ['class' => 'font-weight-bold'])  }}
                    {{ Form::text('fullname', $admin->fullname, ['class' => 'form-control', 'placeholder' => 'Insira aqui o nome completo do administrador']) }}

                    {{ Form::label('email', 'E-mail', ['class' => 'font-weight-bold'])  }}
                    {{ Form::email('email', $admin->email, ['class' => 'form-control', 'placeholder' => 'Insira aqui o e-mail do administrador']) }}

                    {{ Form::label('username', 'Nome de Utilizador', ['class' => 'font-weight-bold'])  }}
                    {{ Form::text('username', $admin->username, ['class' => 'form-control', 'placeholder' => 'O nome de utilizador não deve conter espaços']) }}


                </div>
            </div>
            <!-- FOOTER BUTTONS GO IN THE FOLLOWING SECTION-->
            <div class="modal-footer">
                <a href="/users/view/1" class="btn btn-secondary" data-dismiss="modal"> Cancelar </a>
                {{ Form::submit('Guardar', ['class' => 'btn btn-primary']) }}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endforeach
@endif