
<div class="modal fade" id="register-bookcase" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <!-- MODAL TITLE GOES IN THE FOLLOWING SECTION -->
                <h5 class="modal-title" id="exampleModalLongTitle">
                    Adicionar Estante
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <!-- MAIN MODAL CONTENT GOES IN THE FOLLOWING SECTION -->
            {!! Form::open(['url' => '/library/bookcases/register']) !!}
            @if (!\App\Biblioteca::getInfo()['configMode'])
            <div class="modal-body">
                <div class="form-control">

                    {{ Form::label('cdd', 'CDD Nível 1', ['class' => 'font-weight-bold'])  }}
                    {{ Form::select('cdd', \App\Helpers\Sib::getJsonData('cdd-level-1'), '', ['class' => 'form-control', 'required']) }}

                    {{ Form::label('nShelves', 'Número de Prateleiras', ['class' => 'font-weight-bold'])  }}
                    {{ Form::number('nShelves', 0, ['class' => 'form-control', 'required']) }}

                    <p class="alert alert-primary mt-3">
                        Localização da Estante
                    </p>

                    {{ Form::label('floor', 'Andar', ['class' => 'font-weight-bold'])  }}
                    {{ Form::select('floor', \App\Biblioteca::getFloors(), '', ['class' => 'form-control', 'required', 'id' => 'rb-change-lines']) }}

                    {{ Form::label('line', 'Fila nº', ['class' => 'font-weight-bold'])  }}
                    <select name="line" class="form-control" required id="rb-lines">
                        <option> Selecione a fila </option>
                    </select>

                </div>
            </div>
            @endif
            <!-- FOOTER BUTTONS GO IN THE FOLLOWING SECTION-->
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"> Cancelar </button>
                {{ Form::submit('Registrar', ['class' => 'btn btn-primary']) }}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
