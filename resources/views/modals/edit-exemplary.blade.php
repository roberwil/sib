@if(isset($exemplaries))
    @foreach($exemplaries as $exemplary)
        <div class="modal fade" id="edit-exemplary-{{$exemplary->id}}" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <!-- MODAL TITLE GOES IN THE FOLLOWING SECTION -->
                        <h5 class="modal-title" id="exampleModalLongTitle">
                            Editar Exemplar
                        </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <!-- MAIN MODAL CONTENT GOES IN THE FOLLOWING SECTION -->
                    {!! Form::open(['url' => '/catalog/register/exemplary/' . $exemplary->id ]) !!}
                    <div class="modal-body">
                        <div class="form-control">

                            {{ Form::label('label', 'Etiqueta', ['class' => 'font-weight-bold'])  }}
                            {{ Form::select('label', [ '1' => 'Vermelha', '2' => 'Amarela', '3' => 'Branca' ], $exemplary->label, ['class' => 'form-control', 'required']) }}

                            Prateleira: <span class="text-primary"> {{ $exemplary->shelf != 0 ? $exemplary->shelf : 'Não definida' }}</span>
                            {{ Form::select('shelf', \App\Helpers\Sib::getCompatibleShelves($exemplary->callNumber), $exemplary->label, ['class' => 'form-control', 'required']) }}

                        </div>
                    </div>
                    <!-- FOOTER BUTTONS GO IN THE FOLLOWING SECTION-->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal"> Cancelar </button>
                        {{ Form::submit('Guardar', ['class' => 'btn btn-primary']) }}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>

    @endforeach
@endif