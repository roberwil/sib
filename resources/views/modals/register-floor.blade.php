
<div class="modal fade" id="register-floor" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <!-- MODAL TITLE GOES IN THE FOLLOWING SECTION -->
                <h5 class="modal-title" id="exampleModalLongTitle">
                    Adicionar andar
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <!-- MAIN MODAL CONTENT GOES IN THE FOLLOWING SECTION -->
            {!! Form::open(['url' => '/library/floor/register']) !!}
            @if (!\App\Biblioteca::getInfo()['configMode'])
            @if(!\App\Andar::blockRegister())
                <div class="modal-body">
                    <div class="form-control">

                        {{ Form::label('nLines', 'Número de Filas', ['class' => 'font-weight-bold'])  }}
                        {{ Form::number('nLines', 0, ['class' => 'form-control', 'required']) }}

                    </div>
                </div>
                <!-- FOOTER BUTTONS GO IN THE FOLLOWING SECTION-->
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"> Cancelar </button>
                    {{ Form::submit('Adicionar', ['class' => 'btn btn-primary']) }}
                </div>
            @else
                <p class="alert alert-danger text-center">
                    Atingiu o número limite
                </p>
            @endif
            @endif
            {!! Form::close() !!}
        </div>
    </div>
</div>