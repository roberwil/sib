@if(isset($groups))
@foreach($groups as $group)
<div class="modal fade" id="edit-group-{{$group->id}}" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <!-- MODAL TITLE GOES IN THE FOLLOWING SECTION -->
                <h5 class="modal-title" id="exampleModalLongTitle">
                    Editar Grupo
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <!-- MAIN MODAL CONTENT GOES IN THE FOLLOWING SECTION -->
            {!! Form::open(['url' => '/groups/register/'. $group->id ]) !!}
            <div class="modal-body">
                <div class="form-control">

                    {{ Form::label('description', 'Descrição', ['class' => 'font-weight-bold'])  }}
                    {{ Form::text('description', $group->description, ['class' => 'form-control', 'placeholder' => 'Insira aqui o uma descrição para o grupo', 'required']) }}

                    {{ Form::label('maxNumberBorrow', 'Número Máximo de Empéstimos', ['class' => 'font-weight-bold'])  }}
                    {{ Form::selectRange('maxNumberBorrow', 1, 10, $group->maxNumberBorrow, ['class' => 'form-control', 'required']) }}

                    {{ Form::label('fineBooks', 'Multa de Empréstimo de Livros (por dia)', ['class' => 'font-weight-bold'])  }}
                    {{ Form::number('fineBooks', $group->fineBooks, ['class' => 'form-control', 'required']) }}

                    {{ Form::label('maxTimeBorrow', 'Tempo Máximo de Empréstimo (em dias)', ['class' => 'font-weight-bold'])  }}
                    {{ Form::number('maxTimeBorrow', $group->maxTimeBorrow, ['class' => 'form-control', 'required']) }}

                    {{ Form::label('minTimeBorrow', 'Tempo Mínimo de Empréstimo (em dias)', ['class' => 'font-weight-bold'])  }}
                    {{ Form::number('minTimeBorrow', $group->minTimeBorrow, ['class' => 'form-control', 'required']) }}

                    {{ Form::label('maxReservations', 'Número máximo de Reservas', ['class' => 'font-weight-bold'])  }}
                    {{ Form::number('maxReservations', $group->maxReservations, ['class' => 'form-control', 'required']) }}

                    {{ Form::label('maxTimeReservations', 'Tempo limite da Reserva (em dias)', ['class' => 'font-weight-bold'])  }}
                    {{ Form::number('maxTimeReservations', $group->maxTimeReservations, ['class' => 'form-control', 'required']) }}

                    {{ Form::label('maxRenewals', 'Número máximo de renovações (por empréstimo)', ['class' => 'font-weight-bold'])  }}
                    {{ Form::number('maxRenewals', $group->maxRenewals, ['class' => 'form-control', 'required']) }}

                </div>
            </div>
            <!-- FOOTER BUTTONS GO IN THE FOLLOWING SECTION-->
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"> Cancelar </button>
                {{ Form::submit('Guardar', ['class' => 'btn btn-primary']) }}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endforeach
@endif