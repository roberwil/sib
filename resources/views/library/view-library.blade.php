@extends('layouts.app')
@section('page-description', 'Configurações da Biblioteca')
@section('content')

    <div class="col-md-12">

        @if($library['configMode'])
            <div class="alert alert-danger">
                <p class="text-center font-weight-bold" colspan="6"> Biblioteca não configurada </p>
            </div>
        @else

            <div class="col-md-4">
                <table class="table table-bordered">
                    <tr>
                        <td class="alert alert-success font-weight-bold"> Nome </td>
                        <td> {{  $library['data']['name'] }} </td>
                    </tr>
                </table>
            </div>

            <div class="col-md-4">
                <table class="table table-bordered">
                    <tr>
                        <td class="alert alert-success font-weight-bold"> Prefixo </td>
                        <td> {{  $library['data']['prefix'] }} </td>
                    </tr>
                </table>
            </div>

            <div class="col-md-4">
                <table class="table table-bordered">
                    <tr>
                        <td class="alert alert-success font-weight-bold"> Número de Andares </td>
                        <td> {{  $library['data']['nFloors'] }} </td>
                    </tr>
                </table>
            </div>

        @endif

    </div>

    @if(!$library['configMode'])

        <div class="col-md-12">

            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th scope="col"> Descrição </th>
                    <th scope="col"> Número de Filas </th>
                    <th scope="col"> Editar </th>
                </tr>
                </thead>

                <tbody>

                @if(count($floors) === 0)
                    <tr class="alert alert-dark">
                        <td class="text-center font-weight-bold" colspan="6">
                            Nenhum andar adicionado! Adicione andares em
                            <span class="text-danger">Biblioteca > Adicionar Andar</span>. <br>
                            <span class="text-muted">Nota: Os andares são adicionados de forma sequencial.</span>
                        </td>
                    </tr>
                @endif

                @foreach($floors as $floor)
                    <tr>
                        <td> {{  $floor->description }} </td>
                        <td> {{  $floor->nLines }} </td>
                        <td>
                            <a href="" data-toggle="modal" data-target="#edit-floor-{{ $floor->id }}">
                                <i class="fa fa-edit"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>

        </div>
    @endif

@endsection

