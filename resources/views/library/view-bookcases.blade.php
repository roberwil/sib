@extends('layouts.app')
@section('page-description', 'Gerir Estantes')
@section('content')

    <div class="col-md-12">

        <table class="table table-responsive-md">
            <thead class="thead-dark">
            <tr>
                <th scope="col"> CDD  </th>
                <th scope="col"> Prateleiras </th>
                <th scope="col"> Andar </th>
                <th scope="col"> Fila nº </th>
                <th scope="col"> Editar </th>
                <th scope="col"> Eliminar </th>
            </tr>
            </thead>

            <tbody>

            @if(count($bookcases) === 0)
                <tr class="alert alert-dark">
                    <td class="text-center font-weight-bold" colspan="6"> Sem itens </td>
                </tr>
            @endif

            @foreach($bookcases as $bookcase)
                <tr>
                    <td> {{  \App\Helpers\Sib::resolveCdd('cdd-level-1', $bookcase->cdd, 1) }} </td>
                    <td class="dropdown">
                        <div class="row">
                            <div class="col-md-2">
                                <span class="font-weight-bold"> {{ $bookcase->nShelves }} </span>
                            </div>
                            <div class="col-md-10">
                                <button class="btn btn-outline-secondary btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Opções</button>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="/library/bookcases/shelves/view/{{ $bookcase->id }}"> <i class="fa fa-eye"></i> &nbsp; Ver </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#register-shelf-{{ $bookcase->id }}"> <i class="fa fa-plus"></i> &nbsp; Adicionar </a>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td> {{  \App\Andar::getDescriptionById($bookcase->floor) }} </td>
                    <td> {{  $bookcase->line }} </td>
                    <td> <a href="" data-toggle="modal" data-target="#edit-bookcase-{{ $bookcase->id }}"> <i class="fa fa-edit"></i></a> </td>
                    <td> <a href="/library/bookcases/delete/{{\Illuminate\Support\Facades\Crypt::encrypt( $bookcase->id )}}"><i class="fa fa-remove"></i></a> </td>
                </tr>
            @endforeach
            </tbody>
        </table>

    </div>

@endsection

