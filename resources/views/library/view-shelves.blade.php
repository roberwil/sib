@extends('layouts.app')
@section('page-description', 'Gerir Estantes | Ver Prateleiras')
@section('content')

    {!! \App\Helpers\Sib::buildGoBackBtn('/library/bookcases/view') !!}

    <div class="col-md-12">

        <table class="table">
            <thead class="thead-dark">
            <tr>
                <th scope="col"> CDD Nível 2 </th>
                <th scope="col"> Nº Limite de Livros </th>
                <th scope="col"> Editar </th>
                <th scope="col"> Eliminar </th>
            </tr>
            </thead>

            <tbody>

            @if(count($shelves) === 0)
            <tr class="alert alert-dark">
                <td class="text-center font-weight-bold" colspan="6">
                    <span> Sem itens </span>
                </td>
            </tr>
            @endif

            @foreach($shelves as $shelf)
            <tr>
                <td> {{  \App\Helpers\Sib::resolveCdd('cdd-level-2', $shelf->cdd, 2) }} </td>
                <td> {{  $shelf->maxBooks }} </td>
                <td>
                    <a href="" data-toggle="modal" data-target="#edit-shelf-{{ $shelf->id }}">
                        <i class="fa fa-edit"></i>
                    </a>
                </td>
                <td> <a href="/library/bookcases/shelves/delete/{{ $shelf->id }}"> <i class="fa fa-remove"></i> </a> </td>
            </tr>
            @endforeach

            </tbody>
        </table>

    </div>

@endsection

