<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable
{

    protected $fillable = [
        'fullname', 'email', 'username', 'password'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    /* Static methods */

    public static function getIdByUsername ($username) {
        return DB::table('admins')->where('username', $username)->value('id');
    }

    public static function getFullnameByUsername ($username) {
        return DB::table('admins')->where('username', $username)->value('fullname');
    }

    /* CRUD methods */

    public function retrieveAll () {
        return $this->all();
    }

    public function deleteById ($id) {
        return $this->destroy($id);
    }

    public function editById ($data, $id) {
        return $this->find($id)->update($data);
    }

    public function register ($data) {

        $this->fullname = $data['fullname'];
        $this->username = $data['username'];
        $this->email = $data['email'];
        $this->password = Hash::make($data['password']);

        return $this->save();

    }

    /* Other methods */

}
