<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Isbd4Area extends Model
{

    protected $fillable = [
        'placeOfPublication',
        'nameOfPublisher',
        'dateOfPublication',
        'placeOfPrinting',
        'printerName',
        'dateOfPrinting'
    ];

    public function register ($data) {

        $this->placeOfPublication = $data[0];
        $this->nameOfPublisher = $data[1];
        $this->dateOfPublication = $data[2];
        $this->placeOfPrinting = $data[3];
        $this->printerName = $data[4];
        $this->dateOfPrinting = $data[5];

        $this->save();
        return $this->id;

    }

    public function retrieveById ($id) {
        return $this->find($id)->get();
    }

    public function deleteById ($id) {
        return $this->destroy($id);
    }

    public function editById ($data, $id) {
        return $this->find($id)->update($data);
    }

}
