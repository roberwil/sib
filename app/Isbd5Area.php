<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Isbd5Area extends Model
{

    protected $fillable = [
        'specificationOfMaterialAndExtension',
        'otherPhysicalDetails',
        'dimensions',
        'declarationOfAccompanyingMaterial'
    ];

    public function register ($data) {

        $this->specificationOfMaterialAndExtension = $data[0];
        $this->otherPhysicalDetails = $data[1];
        $this->dimensions = $data[2];
        $this->declarationOfAccompanyingMaterial = $data[3];

        $this->save();
        return $this->id;

    }

    public function retrieveById ($id) {
        return $this->find($id)->get();
    }

    public function deleteById ($id) {
        return $this->destroy($id);
    }

    public function editById ($data, $id) {
        return $this->find($id)->update($data);
    }

}
