<?php

namespace App;

use App\Helpers\Sib;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Estante extends Model
{

    protected $fillable = [
        'cdd', 'nShelves', 'floor', 'line'
    ];

    public static function getForLocation ($floor, $line) {

        $data = DB::table('estantes')->where('floor', '=', $floor)->where('line', '=', $line)->get();
        $result = [];

        $result['0'] = 'Escolha a estante';

        foreach ($data as $item)
            $result[$item->id] = $item->description;

        return $result;
    }

    public function retrieveAll () {
        return $this->all();
    }

    public function deleteById ($id) {
        return $this->destroy($id);
    }

    public function editById ($data, $id) {
        $value = $data['cdd'] . ' ' . $data['floor'] . '.' . $data['line'];
        Sib::updateAttr('estantes', 'description', $id, $value);
        return $this->find($id)->update($data);
    }

    public function register ($data) {

        $this->cdd = $data['cdd'];
        $this->nShelves = $data['nShelves'];
        $this->floor = $data['floor'];
        $this->line = $data['line'];
        $this->description = $data['cdd'] . ' ' . $data['floor'] . '.' . $data['line'];

        return $this->save();

    }


}
