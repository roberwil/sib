<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Biblioteca extends Model
{

    protected $fillable = [
        'name', 'prefix', 'nFloors'
    ];

    public static function getInfo () {

        $dbResult = Biblioteca::all();
        $configMode = count($dbResult) == 0 ? true : false;

        // edit mode
        if (!$configMode) {
            $data = $dbResult[0];
            $result = [
                'configMode' => $configMode,
                'data' => $data
            ];
        }
        // config mode
        else {
            $result = [
                'configMode' => $configMode
            ];
        }

        return $result;

    }

    public static function getFloors () {

        $id = Biblioteca::all()->first()->id;
        $data = DB::table('andars')->where('libraryId', '=', $id)->get();

        $result[''] = 'Escolha o Andar';

        foreach($data as $item) {
            $result[$item->id] = $item->description;
        }

        return $result;

    }

    public function deleteFloors ($data) {

        $floorObj = new Andar;
        $countFloor = count($floorObj->retrieveAll());

        if ($countFloor == $data['nFloors'] || $countFloor < $data['nFloors']) {
            return;
        }

        $limit = $data['nFloors'] - $countFloor;
        $limit = $limit < 0 ? $limit*(-1) : $limit;

        for ($i = 0; $i < $limit; $i++) {
            $floorId = $floorObj->retrieveAll()->last()->id;
            $floorObj->deleteById($floorId);
        }

        return;

    }

    public function retrieveAll () {
        return $this->all();
    }

    public function deleteById ($id) {
        return $this->destroy($id);
    }

    public function editById ($data, $id) {
        $this->deleteFloors($data);
        return $this->find($id)->update($data);
    }

    public function register ($data) {

        $this->name = $data['name'];
        $this->prefix = $data['prefix'];
        $this->nFloors = $data['nFloors'];

        return $this->save();

    }

}
