<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Grupo extends Model
{


    protected $fillable = [
        'description',
        'maxNumberBorrow',
        'fineBooks',
        'maxTimeBorrow',
        'minTimeBorrow',
        'maxReservations',
        'maxRenewals',
        'maxTimeReservations'
    ];

    protected $hidden = [];

    public static function retriveDescriptionById ($id) {

        $group = new Grupo;
        return $group->find($id)->description;

    }

    public static function retrieveGroupsArray () {

        $grupo = new Grupo;
        $groups = $grupo->retrieveAll();
        $userGroups = [];

        foreach ($groups as $groupItem) {
            $userGroups[$groupItem->id] = $groupItem->description;
        }

        return $userGroups;

    }

    public function register ($data) {

        $this->description = $data['description'];
        $this->maxNumberBorrow = $data['maxNumberBorrow'];
        $this->fineBooks = $data['fineBooks'];
        $this->maxTimeBorrow = $data['maxTimeBorrow'];
        $this->minTimeBorrow = $data['minTimeBorrow'];
        $this->maxReservations = $data['maxReservations'];
        $this->maxTimeReservations = $data['maxTimeReservations'];
        $this->maxRenewals = $data['maxRenewals'];

        return $this->save();

    }

    public function retrieveAll () {
        return $this->all();
    }

    public function deleteById ($id) {
        return $this->destroy($id);
    }

    public function editById ($data, $id) {
        return $this->find($id)->update($data);
    }

}
