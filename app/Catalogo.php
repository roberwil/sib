<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Catalogo extends Model
{

    protected $fillable = [
        'material',
        'area1',
        'area2',
        'area3',
        'area4',
        'area5',
        'area6',
        'area7',
        'area8'
    ];

    public function register ($data) {

        $this->material = $data['material'];
        $this->area1 = $data['area1'];
        $this->area2 = $data['area2'];
        $this->area3 = $data['area3'];
        $this->area4 = $data['area4'];
        $this->area5 = $data['area5'];
        $this->area6 = $data['area6'];
        $this->area7 = $data['area7'];
        $this->area8 = $data['area8'];

        return $this->save();

    }

    public function deleteById ($id) {

        $data = $this->find($id)->get();
        $this->destroy($id);

        // remove all ISBD fields related to this catalog
        $result  = Isbd1Area::destroy($data['area1']);
        $result &= Isbd2Area::destroy($data['area2']);
        $result &= Isbd3Area::destroy($data['area3']);
        $result &= Isbd4Area::destroy($data['area4']);
        $result &= Isbd5Area::destroy($data['area5']);
        $result &= Isbd6Area::destroy($data['area6']);
        $result &= Isbd7Area::destroy($data['area7']);
        $result &= Isbd8Area::destroy($data['area8']);

        return $result;

    }

}
