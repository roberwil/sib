<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Isbd7Area extends Model
{

    protected $fillable = [

        'contentMediaTypeArea',
        'titleDeclarationAreaResponsibility',
        'editionAreaBibliographicHistoryResource',
        'materialTypeResourceSpecificArea',

        'publicationArea',
        'physicalDescriptionArea',
        'seriesArea',
        'content',

        'resourceIdentifierAvailabilityAreaTerms',
        'questionPartIterationBaseDescription',
        'otherNotes',
        'copyingHands'

    ];

    public function register ($data) {
        
        $this->contentMediaTypeArea = $data[0];
        $this->titleDeclarationAreaResponsibility = $data[1];
        $this->editionAreaBibliographicHistoryResource = $data[2];
        $this->materialTypeResourceSpecificArea = $data[3];
        
        $this->publicationArea = $data[4];
        $this->physicalDescriptionArea = $data[5];
        $this->seriesArea = $data[6];
        $this->content = $data[7];
        
        $this->resourceIdentifierAvailabilityAreaTerms = $data[8];
        $this->questionPartIterationBaseDescription = $data[9];
        $this->otherNotes = $data[10];
        $this->copyingHands = $data[11];
        
        $this->save();
        return $this->id;
        
    }

    public function retrieveById ($id) {
        return $this->find($id)->get();
    }

    public function deleteById ($id) {
        return $this->destroy($id);
    }

    public function editById ($data, $id) {
        return $this->find($id)->update($data);
    }

}
