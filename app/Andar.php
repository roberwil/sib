<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Andar extends Model
{

    protected $fillable = [
        'description', 'nLines', 'libraryId'
    ];

    public static function blockRegister () {
        return count(Andar::all()) == Biblioteca::getInfo()['data']['nFloors'];
    }

    public static function getDescriptionById ($id) {
        return Andar::find($id)->description;
    }

    public static function getMaxLine ($floorId) {

        $maxLine = Andar::find($floorId)->nLines;

        return [
            'maxLine' => $maxLine
        ];

    }

    public static function get () {

        $floors = Andar::all();
        $data['0'] = 'Escolha o andar';

        foreach ($floors as $floor)
            $data[$floor->id] = $floor->description;

        return $data;

    }

    public function retrieveAll () {
        return $this->all();
    }

    public function deleteById ($id) {
        return $this->destroy($id);
    }

    public function editById ($data, $id) {
        return $this->find($id)->update($data);
    }

    public function register ($data) {

        $number = count($this->retrieveAll()) + 1;
        $this->description = $number . 'º Andar';
        $this->nLines = $data['nLines'];
        $this->libraryId = Biblioteca::getInfo()['data']['id'];

        return $this->save();

    }

}
