<?php
/**
 * Created by PhpStorm.
 * User: roberwil
 * Date: 3/23/18
 * Time: 6:03 AM
 */

namespace App\Helpers;

use App\Prateleira;
use Illuminate\Support\Facades\DB;

class Sib
{

    public static function getAttribute ($table, $attr, $id) {
        return DB::table($table)->where('id', $id)->value($attr);
    }

    public static function updateAttr ($table, $attr, $id, $value) {
        return DB::table($table)->where('id', $id)->update([ $attr => $value]);
    }

    public static function containsExtension ($item, $extension) {

        $start = strpos($item, ".");

        if ($start !== false) {
            $start += 1;
            $itemExtension = substr($item, $start);
            return $itemExtension == $extension ? true : false;
        }

        return false;

    }

    public static function getJsonData ($fileName, $opt = 0) {

        $name = self::containsExtension($fileName, 'json') ? storage_path() . '/json/' . $fileName : storage_path() . '/json/' . $fileName . '.json';

        $data = json_decode(file_get_contents($name), true);
        $result = [];

        if ($opt == 0) {
            foreach ($data as $key => $value) {
                $result[$key] = $key . ' ' . $value;
            }
        } else {
            $result = $data;
        }

        return $result;

    }

    public static function createSelect($array, $selected) {

        $html = '';

        foreach ($array as $key => $value) {
            if ($key == $selected) {
                $html .= '<option selected="selected" value="' . $key .'">' . $value . '</option>';
            } else {
                $html .= '<option value="' . $key .'">' . $value . '</option>';
            }
        }

        return $html;

    }

    public static function resolveCdd($filename, $keyId, $level) {

        $cdd = Sib::getJsonData($filename, 1);

        if ($level == 1 || $level == 2) {

            $description = '';

            foreach ($cdd as $key => $value) {
                if ($key == $keyId) {
                    $description = $value;
                    break;
                }
            }

            return $keyId . ' ' . $description;

        }

    }

    public static function resolveCddLevel($cdd) {
        if ($cdd[1] == 0 && $cdd[2] == 0) {
            return 1;
        } else if ($cdd[2] == 0) {
            return 2;
        } else {
            return 3;
        }
    }

    public static function checkCddDependency($cdd1, $cdd2, $startLevel) {


        if ($startLevel == 1) {
            return $cdd1[0] == $cdd2[0] ? true : false;
        }

        return $cdd1[1] == $cdd2[1] ? true : false;

    }

    public static function classifyMaterialHelper ($cdds) {

        $cdd1Level = self::resolveCddLevel($cdds[0]);
        $cdd2Level = self::resolveCddLevel($cdds[1]);

        // both are on the same level
        if ($cdd1Level == $cdd2Level) {
            // if they are both level 1
            if ($cdd1Level == 1) {
                // if they belong to the same division
                if ($cdds[0][0] == $cdds[1][0])
                    return $cdds[0];
                // if they belong to different division
                else
                    return $cdds[0] . '.' . $cdds[1][0];
            }
            // if they are both level 2
            else if ($cdd1Level == 2) {
                // if they are from the same division 1
                if ($cdds[0][1] == $cdds[1][1]) {
                    return $cdds[1];
                }
                // if they are NOT from the same division 1
                else {
                    return $cdds[0] . '.' . $cdds[1][0] . $cdds[1][1];
                }
            }
            // if they are both level 3
            else {
                // if they are from the same division 1
                if ($cdds[0] == $cdds[1]) {
                    return $cdds[0];
                }
                // if they are NOT from the same division 1
                else {
                    return $cdds[0] . '.' . $cdds[1];
                }
            }

        }
        // cdd1 level is lesser than cdd2 level
        else if ($cdd1Level < $cdd2Level) {

            if ($cdd2Level == 3)
                return $cdds[0] . '.' . $cdds[1];

            if (self::checkCddDependency($cdds[0], $cdds[1], 1) || self::checkCddDependency($cdds[0], $cdds[1], 2)) {
                return $cdds[1];
            }

            return $cdds[0] . '.' . $cdds[1][0] . $cdds[1][1];

        }
        // cdd1 level is greater than cdd2 level
        else {

            if ($cdd2Level == 1)
                return $cdds[0] . '.' . $cdds[1][0];

            if (self::checkCddDependency($cdds[1], $cdds[0], 1) || self::checkCddDependency($cdds[1], $cdds[0], 2)) {
                return $cdds[1];
            }

            return $cdds[1] . '.' . $cdds[0][0] . $cdds[0][1];

        }

    }

    public static function classifyMaterial($cdds) {

        if (count($cdds) == 1) {
            return $cdds[0];
        }

        else if (count($cdds) == 2) {
            return self::classifyMaterialHelper($cdds);
        }

        else {

            if ($cdds[0] == $cdds[1] && $cdds[1] == $cdds[2]) {
                return $cdds[0];
            }

            $cddHelper = [];
            $cddHelper[0] = $cdds[0];
            $cddHelper[1] = $cdds[1];
            $partial = self::classifyMaterialHelper($cddHelper);

            if (strlen($partial) == 3) {
                // level 1
                if (self::resolveCddLevel($cdds[2]) == 1) {
                    return $partial . '.' . $cdds[2][0];
                }
                // level 2
                else if (self::resolveCddLevel($cdds[2]) == 2) {
                    return $partial . '.' . $cdds[2][0] . $cdds[2][1];
                }
                // level 3
                else {
                    return $partial . '.' . $cdds[2];
                }
            }
            else if (strlen($partial) == 5) {
                // level 1
                if (self::resolveCddLevel($cdds[2]) == 1) {
                    return $partial . ' ' . $cdds[2][0];
                }
                // level 2
                else if (self::resolveCddLevel($cdds[2]) == 2) {
                    // same family
                    if ($partial[4] == $cdds[2][0]) {
                        return $partial . $cdds[2][1];
                    }
                    // apart bros
                    else {
                        return $partial . ' ' . $cdds[2][0] . $cdds[2][1];
                    }
                }
                // level 3
                else {
                    // same family
                    if ($partial[4] == $cdds[2][0]) {
                        return $partial . $cdds[2][1] . $cdds[2][2];
                    }
                    // apart bros
                    else {
                        return $partial . ' ' . $cdds[2];
                    }
                }
            }
            else if (strlen($partial) == 7) {
                // level 1
                if (self::resolveCddLevel($cdds[2]) == 1) {
                    return $partial . ' ' . $cdds[2][0];
                }
                // level 2
                else if (self::resolveCddLevel($cdds[2]) == 2) {
                    // same family
                    if ($partial[5] == $cdds[2][0] && $partial[6] == $cdds[2][1]) {
                        return $partial . $cdds[2][2];
                    }
                    // apart bros
                    else {
                        return $partial . ' ' . $cdds[2][2];
                    }
                }
                // level 3
                else {
                    return $partial . ' ' . $cdds[2];
                }
            }
            else {
                // level 1
                if (self::resolveCddLevel($cdds[2]) == 1) {
                    return $partial . ' ' . $cdds[2][0];
                }
                // level 2
                else if (self::resolveCddLevel($cdds[2]) == 2) {
                    return $partial . ' ' . $cdds[2][0] . $cdds[2][1];
                }
                // level 3
                else {
                    return $partial . ' ' . $cdds[2];
                }

            }

        }


    }

    public static function resolveProfile($code, $id) {

        if ($code == 'librarian') {
            $level = self::getAttribute('bibliotecarios', 'level', $id);
            return "Bibliotecário Nível " . $level;
        }

        if ($code == 'reader') {
            return "Leitor";
        }

        return "Admnistrador";

    }

    public static function resolveMaterialType ($code) {

        $types = self::getJsonData('type-of-materials', 1);

        foreach ($types as $key => $value)
            if ($code == $key)
                return $value;

        return "Tipo desconhecido";

    }

    public static function buildGoBackBtn ($url) {
        $html = '<div class="col-md-12">
                    <a href="' . $url .  '" class="btn btn-outline-dark mb-3">
                        <i class="fa fa-chevron-left"></i>  &nbsp; Voltar
                    </a>
                </div>';
        return $html;
    }

    public static function formatFullname($fullname) {

        if (strpos($fullname, ",") !== false) {
            return $fullname;
        }

        $array = explode(" ", $fullname);
        $lastName = array_pop($array) . ', ';

        return $lastName . implode(" ", $array);

    }

    public static function getCutterSandbornCode ($fullname) {

        $fullname = self::formatFullname($fullname);

        $commaPos = strpos($fullname, ",");
        $fpartial = substr($fullname, 0, $commaPos + 4);
        $spartial = substr($fullname, 0, $commaPos);

        $cutterSandborn =  self::getJsonData('cutter-sandborn', 1);

        // Rober, C. like names
        foreach ($cutterSandborn as $key => $value)
            if ($fpartial == $value)
                return $key;

        $len = strlen($spartial);

        // other names
        for ($i = 0; $i < $len; $i += 1) {

            foreach ($cutterSandborn as $key => $value)
                if ($spartial == $value)
                    return $key;

            $spartial = substr($spartial, 0, -1);

        }

    }

    public static function generateCallNumber ($materialId, $numberOfExemplary) {

        $fullname = self::getAttribute('materials', 'authors', $materialId);
        $classification = self::getAttribute('materials', 'classification', $materialId);
        $editionYear = self::getAttribute('materials', 'editionYear', $materialId);
        $title = self::getAttribute('materials', 'title', $materialId);

        $array = explode(" ", $title);
        $titleCode = '';

        if (count($array) == 1)
            $titleCode = $array[0][0];
        else {
            if (strlen($array[0]) == 1) {
                $titleCode = $array[1][0];
            }
        }

        $titleCode = strtolower($titleCode);

        return $classification . ' ' . self::getCutterSandbornCode($fullname) . $titleCode . ' ' . $editionYear . ' ' . 'ex.' . $numberOfExemplary;

    }

    public static function resolveLabel ($code) {

        switch ($code) {
            case 1:
                return 'Vermelha';
            case 2:
                return 'Amarela';
            case 3:
                return 'Branca';
        }

    }

    public static function getCompatibleShelves ($callNumber) {

        $cddexemplary = $callNumber[0] . $callNumber[2] . $callNumber[2];
        $cddexemplaryLevel = self::resolveCddLevel($cddexemplary);

        $shelf = new Prateleira;
        $candidates = $shelf->retrieveAll();
        $result = [];

        foreach ($candidates as $item) {

            $cdd = $item->cdd;

            if ($cddexemplaryLevel == 2 || $cddexemplaryLevel == 1) {
                if ($cddexemplary[0] == $cdd[0] & $cddexemplary[1] == $cdd[1]) {
                    $result[$item->id] = $item->cdd . ' ' . self::getAttribute('estantes', 'description', $item->bookcase);
                }
            }

            if ($cddexemplaryLevel == 1) {
                if ($cddexemplary[0] == $cdd[0]) {
                    $result[$item->id] = $item->cdd . ' ' . self::getAttribute('estantes', 'description', $item->bookcase);
                }
            }

        }

        return $result;


    }


}