<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Isbd3Area extends Model
{

    protected $fillable = [
        'mathematicalData',
        'statementOfMusicFormat',
        'numbering'
    ];

    public function register ($data) {

        $this->mathematicalData = $data[0];
        $this->statementOfMusicFormat = $data[1];
        $this->numbering = $data[2];

        $this->save();
        return $this->id;

    }

    public function retrieveById ($id) {
        return $this->find($id)->get();
    }

    public function deleteById ($id) {
        return $this->destroy($id);
    }

    public function editById ($data, $id) {
        return $this->find($id)->update($data);
    }

}
