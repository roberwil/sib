<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Isbd8Area extends Model
{

    protected $fillable = [
        'resourceIdentifier',
        'keyTitle',
        'termsAvailability'
    ];

    public function register ($data) {

        $this->resourceIdentifier = $data[0];
        $this->keyTitle = $data[1];
        $this->termsAvailability = $data[2];

        $this->save();
        return $this->id;

    }

    public function retrieveById ($id) {
        return $this->find($id)->get();
    }

    public function deleteById ($id) {
        return $this->destroy($id);
    }

    public function editById ($data, $id) {
        return $this->find($id)->update($data);
    }

}
