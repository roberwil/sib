<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Exemplar extends Model
{

    protected $fillable = [
        'material',
        'number', //we get it automatically
        'label', //1 - red; 2 - yellow; 3 - white
        'callNumber', //automatically
        'available', //automatically available if it is not labelled red
        'shelf' //after we add it to a shelf
    ];

    public function retrieveAllByMaterialId($id) {
        return DB::table('exemplars')->where('material', '=', $id)->get();
    }

    public function deleteById ($id) {
        return $this->destroy($id);
    }

    public function editById ($data, $id) {
        return $this->find($id)->update($data);
    }

    public function register ($data) {

        $this->material = $data['material'];
        $this->number = $data['number'];
        $this->label = $data['label'];
        $this->callNumber = $data['callNumber'];
        $this->available = $data['available'];

        return $this->save();

    }


}
