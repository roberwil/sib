<?php

namespace App\Http\Middleware;

use Closure;

class GhostBuster
{

    protected $redirectedPath = '/';
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (!session()->has('user')) {
            return redirect('/login');
        }

        return $next($request);

    }
}
