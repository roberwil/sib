<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CirculationController extends Controller
{


    public function viewBorrow () {
        return view('circulation.borrow-books');
    }

    public function viewDeliver () {
        return view('circulation.retrieve-books');
    }


}
