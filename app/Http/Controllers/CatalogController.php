<?php

namespace App\Http\Controllers;

use App\Catalogo;
use App\Exemplar;
use App\Isbd1Area;
use App\Isbd2Area;
use App\Isbd3Area;
use App\Isbd4Area;
use App\Isbd5Area;
use App\Isbd6Area;
use App\Isbd7Area;
use App\Isbd8Area;
use App\Helpers\Sib;
use App\Material;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class CatalogController extends Controller
{

    public function registerArea1 ($data) {
        $isbd = new Isbd1Area;
        return $isbd->register($data);
    }

    public function registerArea2 ($data) {
        $isbd = new Isbd2Area;
        return $isbd->register($data);
    }

    public function registerArea3 ($data) {
        $isbd = new Isbd3Area;
        return $isbd->register($data);
    }

    public function registerArea4 ($data) {
        $isbd = new Isbd4Area;
        return $isbd->register($data);
    }

    public function registerArea5 ($data) {
        $isbd = new Isbd5Area;
        return $isbd->register($data);
    }

    public function registerArea6 ($data) {
        $isbd = new Isbd6Area;
        return $isbd->register($data);
    }

    public function registerArea7 ($data) {
        $isbd = new Isbd7Area;
        return $isbd->register($data);
    }

    public function registerArea8 ($data) {
        $isbd = new Isbd8Area;
        return $isbd->register($data);
    }

    public function registerNew (Request $request, $step = 0) {

        switch ($step) {

            case 1:

                $material = new Material;
                $data = [];
                $classification = Sib::classifyMaterial($request->input('cdd'));

                $data['classification'] = $classification;
                $data['registeredBy'] = session('user');
                $data['nExemplary'] = $request->input('nExemplary');
                $data['type'] = $request->input('type');

                $data['title'] = $request->input('title');
                $data['authors'] = $request->input('authors');
                $data['edition'] = $request->input('edition');
                $data['editionYear'] = $request->input('editionYear');
                $data['publisher'] = $request->input('publisher');
                $data['publishingYear'] = $request->input('publishingYear');


                $materialId = $material->register($data);
                session(['materialId' => $materialId]);
                session()->save();

                return redirect('/catalog/register/new/view/2');
                break;

            case 2:

                /** deal with area 1 */
                $area1 = $this->registerArea1($request->input('area1'));

                /** deal with area 2 */
                $area2 = $this->registerArea2($request->input('area2'));

                /** deal with area 3 */
                $area3 = $this->registerArea3($request->input('area3'));

                /** deal with area 4 */
                $area4 = $this->registerArea4($request->input('area4'));

                /** deal with area 5 */
                $area5 = $this->registerArea5($request->input('area5'));

                /** deal with area 6 */
                $area6 = $this->registerArea6($request->input('area6'));

                /** deal with area 7 */
                $area7 = $this->registerArea7($request->input('area7'));

                /** deal with area 8 */
                $area8 = $this->registerArea8($request->input('area8'));

                /** insert catalog */
                $catalog = new Catalogo;
                $material = session('materialId');

                $catalogData = [
                    'material' => $material,
                    'area1' => $area1,
                    'area2' => $area2,
                    'area3' => $area3,
                    'area4' => $area4,
                    'area5' => $area5,
                    'area6' => $area6,
                    'area7' => $area7,
                    'area8' => $area8,
                ];

                $catalog->register($catalogData);

                //discard the material id after it is all done
                if (session('materialId'))
                    session()->forget('materialId');

                // redirect to view
                return redirect('/catalog/view');

                break;

        }

    }

    public function registerExemplary(Request $request, $id = 0) {

        $exemplar =  new Exemplar;

        // editing
        if ($id != 0) {
            $data['label'] = $request->input('label');
            $data['shelf'] = $request->input('shelf');
            $data['available'] = true;
            $exemplar->editById($data, $id);
            $material = Sib::getAttribute('exemplars', 'material', $id);
            return redirect('/catalog/view/exemplary/' . $material);
        }

        // validating
        $rules = [
            'material' => 'required',
            'label' => 'required'
        ];

        Validator::make($request->input(), $rules)->validate();

        $data['label'] = $request->input('label');
        $data['material'] = $request->input('material');
        $data['number'] = count($exemplar->retrieveAllByMaterialId($request->input('material'))) + 1;
        $data['callNumber'] = Sib::generateCallNumber($request->input('material'), $data['number']);

        if ($request->input('label') == 1) {
            $data['available'] = false;
        } else {
            $data['available'] = true;
        }

        $exemplar->register($data);

        return redirect('/catalog/view/exemplary/' . $data['material']);

    }

    public function viewRegisterNew($step = 1) {

        switch ($step) {
            case 1:
                return view('catalog.form-step-1');
                break;
            case 2:
                return view('catalog.form-step-2');
                break;
            default:
                return view('catalog.form-step-1');

        }

    }

    public function viewSearch() {
        return view('main');
    }

    public function viewCatalog() {
        return redirect('catalog.view-catalog');
    }

    public function viewExemplary ($material = 0) {
        $exemplary = new Exemplar;
        $exemplaries = $exemplary->retrieveAllByMaterialId($material);
        return view('catalog.view-exemplary', ['exemplaries' => $exemplaries]);
    }

    public function viewMaterial () {
        $material  = new Material;
        $materials = $material->retrieveAll();
        return view('catalog.view-material', ['materials' => $materials]);
    }

    public function editCatalog() {
        return view('catalog.edit-catalog');
    }

    public function vieEditCatalog ($id) {

    }

    public function editMaterial (Request $request, $id) {

        $material = new Material;
        $exemplaries = DB::table('exemplars')->where('material', '=', $id)->get();

        foreach ($exemplaries as $exemplary) {
            $callNumber = Sib::generateCallNumber($id, $exemplary->number);
            Sib::updateAttr('exemplars', 'callNumber', $exemplary->id, $callNumber);
        }

        $material->editById($request->input(), $id);
        return redirect('/catalog/view/material');

    }

    public function viewEditMaterial ($id) {
        $material = DB::table('materials')->where('id', '=', $id)->get()->first();
        return view('catalog.edit-material', ['material' => $material]);
    }

    public function deleteItem ($which, $id) {

        switch ($which) {
            case 1:
                // material
                break;
            case 2:
                // catalog
                break;
            case 3:
                // exemplary
                $exemplary = new Exemplar;
                $material = Sib::getAttribute('exemplars', 'material', $id);
                $exemplary->deleteById($id);
                return redirect('/catalog/view/exemplary/' . $material);
                break;
        }

    }
}
