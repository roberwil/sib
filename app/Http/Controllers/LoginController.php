<?php

namespace App\Http\Controllers;

use App\Admin;
use App\Bibliotecario;
use App\Leitor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{

    use AuthenticatesUsers;

    public function viewLogin () {
        return view('login');
    }

    public function login (Request $request) {;

        $username = $request->input('username');
        $password = $request->input('password');

        $rules = [
            'username' => 'required',
            'password' => 'required'
        ];

        Validator::make($request->input(), $rules)->validate();

        // maybe an admin is logging in
        if (Auth::guard('admin')->attempt(['username' => $username, 'password' => $password])) {
            session(['profile' => 'admin', 'user' => Admin::getIdByUsername($username), 'fullname' => Admin::getFullnameByUsername($username)]);
            session()->save();
            return redirect('/default');
        }
        // maybe a librarian is logging in
        else if (Auth::guard('librarian')->attempt(['username' => $username, 'password' => $password])) {
            session(['profile' => 'librarian', 'user' => Bibliotecario::getIdByUsername($username), 'fullname' => Bibliotecario::getFullnameByUsername($username)]);
            return redirect('/default');
        }
        // maybe a reader is logging in
        else if (Auth::guard('reader')->attempt(['username' => $username, 'password' => $password])) {
            session(['profile' => 'reader', 'user' => Leitor::getIdByUsername($username), 'fullname' => Leitor::getFullnameByUsername($username)]);
            return redirect('/default');
        }
        // some smart guy is trying to log in
        else {
            return view('login', ['error' => 'Nome de utilizador ou palavra-passe errados!']);
        }

    }

    public function logout () {
        Auth::logout();
        session()->forget(['profile', 'user', 'fullname']);
        return redirect('/');
    }

}
