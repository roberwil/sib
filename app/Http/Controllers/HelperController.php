<?php

namespace App\Http\Controllers;

use App\Andar;
use App\Estante;
use App\Helpers\Sib;
use App\Prateleira;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HelperController extends Controller
{

    public function getLinesByFloorId ($floorId) {
        return response()->json(Andar::getMaxLine($floorId));
    }

    public function getCDD ($level) {
        return response()->json(Sib::getJsonData('cdd-level-' . $level));
    }

    public function defaultPage () {
        return view('default');
    }

    public function test($item)
    {
        dd(Sib::resolveMaterialType($item));
    }

    public function exemplaryLocationHandler($which, $id, $helperId = 0) {

        switch ($which) {
            case 1:
                return response()->json(Andar::getMaxLine($id));
            case 2:
                return response()->json(Estante::getForLocation($helperId, $id));
            case 3:
                return response()->json(Prateleira::getForLocation($id));
                break;
        }

    }

    public function checkBook ($id) {
        return response()->json(['cool' => Sib::getAttribute('exemplars', 'available', $id)]);
    }

    public function checkUser ($id) {
        // to do
    }

}
