<?php

namespace App\Http\Controllers;

use App\Admin;
use App\Andar;
use App\Biblioteca;
use App\Bibliotecario;
use App\Estante;
use App\Grupo;
use App\Leitor;
use App\Prateleira;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Validator;

class ConfigController extends Controller
{

    public function __construct()
    {
        $this->middleware('ghostbuster');
    }

    /** Library */

    public function viewBookcases () {
        $bookcase = new Estante;
        $bookcases = $bookcase->retrieveAll();
        return view('library.view-bookcases', ['bookcases' => $bookcases]);
    }

    public function viewShelves ($id) {
        $shelf = new Prateleira;
        $shelves = $shelf->retrieveAllByBookcasId($id);
        return view('library.view-shelves', ['shelves' => $shelves]);
    }

    public function viewLibrary () {
        $library = Biblioteca::getInfo();
        $floor = new Andar;
        $floors = $floor->retrieveAll();
        return view('library.view-library', ['library' => $library, 'floors' => $floors]);
    }

    public function deleteShelf ($id) {
        $shelf = new Prateleira;
        $bookcase = $shelf->find($id)->bookcase;
        $shelf->deleteById($id);
        return redirect('/library/bookcases/shelves/view/' . $bookcase);
    }

    public function registerShelf (Request $request, $id = 0) {

        $shelf = new Prateleira;

        // editing
        if ($id != 0) {
            $shelf->editById($request->input(), $id);
            return redirect('/library/bookcases/shelves/view/' . $request->input('bookcase'));
        }

        // validating
        $rules = [
            'cdd' => 'required',
            'maxBooks' => 'required',
            'bookcase' => 'required'
        ];

        Validator::make($request->input(), $rules)->validate();

        //saving
        $shelf->register($request->input());

        return redirect('/library/bookcases/shelves/view/' . $request->input('bookcase'));

    }

    public function deleteBookcase ($id) {
        $bookcase = new Estante;
        $bookcaseId = Crypt::decrypt($id);
        $bookcase->deleteById($bookcaseId);
        return redirect('/library/bookcases/view');
    }

    public function registerBookcase (Request $request, $id = 0) {

        $bookcase = new Estante;

        // editing
        if ($id != 0) {
            $bookcase->editById($request->input(), $id);
            return redirect('/library/bookcases/view');
        }

        // validating
        $rules = [
            'cdd' => 'required',
            'nShelves' => 'required',
            'floor' => 'required'
        ];

        Validator::make($request->input(), $rules)->validate();

        //saving
        $bookcase->register($request->input());

        return redirect('/library/bookcases/view');

    }

    public function registerFloor (Request $request, $id = 0) {

        $floor = new Andar;

        // validating
        $rules = [
            'nLines' => 'required'
        ];

        Validator::make($request->input(), $rules)->validate();

        // editing
        if ($id != 0) {
            $floor->editById($request->input(), $id);
            return redirect('/library/view');
        }

        // saving
        $floor->register($request->input());
        return redirect('/library/view');

    }

    public function configLibrary (Request $request, $id = 0) {

        $library = new Biblioteca;

        // validating
        $rules = [
            'name' => 'required',
            'prefix' => 'required',
            'nFloors' => 'required'
        ];

        Validator::make($request->input(), $rules)->validate();

        // editing
        if ($id != 0) {
            $library->editById($request->input(), $id);
            return redirect('/library/view');
        }

        // saving
        $library->register($request->input());
        return redirect('/library/view');

    }

    /** Groups */

    public function deleteGroups ($id) {

        $group = new Grupo;
        $groupId = Crypt::decrypt($id);
        $group->deleteById($groupId);

        return redirect('/groups/view');

    }

    public function registerGroup (Request $request, $id = 0) {

        $group = new Grupo;

        // validating
        $rules = [
            'description' => 'required',
            'maxNumberBorrow' => 'required',
            'fineBooks' => 'required',
            'maxTimeBorrow' => 'required',
            'minTimeBorrow' => 'required',
            'maxReservations' => 'required',
            'maxRenewals' => 'required',
            'maxTimeReservations' => 'required'
        ];

        Validator::make($request->input(), $rules)->validate();

        // updating
        if ($id != 0) {
            $group->editById($request->input(),$id);
            return redirect('/groups/view');
        }

        // saving
        $group->register($request->input());

        return redirect('/groups/view');

    }

    public function viewGroups () {
        $group = new Grupo;
        $groups = $group->retrieveAll();
        return view('users.view-groups', ['groups' => $groups]);
    }

    /** Users */

    public function deleteUsers ($id, $user) {

        $route = '/users/view/' . $id;
        $userId = Crypt::decrypt($user);

        $admin = new Admin;
        $librarian = new Bibliotecario;
        $reader = new Leitor;


        switch ($id) {
            case 1:
                $admin->deleteById($userId);
                break;
            case 2:
                $librarian->deleteById($userId);
                break;
            case 3:
                $reader->deleteById($userId);
                break;
        }

        return redirect($route);

    }

    public function viewUsers ($id) {

        $admin = new Admin;
        $librarian = new Bibliotecario;
        $reader = new Leitor;

        switch ($id) {
            case 1:
                $admins = $admin->retrieveAll();
                return view('users.view-admins', ['admins' => $admins]);
                break;
            case 2:
                $librarians = $librarian->retrieveAll();
                return view('users.view-librarians', ['librarians' => $librarians]);
                break;
            case 3:
                $readers = $reader->retrieveAll();
                return view('users.view-readers', ['readers' => $readers]);
                break;
        }

    }

    public function registerAdmin (Request $request, $user = 0) {


        $admin = new Admin;

        // updating
        if ($user != 0) {
            $admin->editById($request->input(), $user);
            return redirect('/users/view/1');
        }

        // validating
        $rules = [
            'fullname' => 'required',
            'username' => 'required|unique:admins',
            'email' => 'required|email|unique:admins'
        ];

        Validator::make($request->input(), $rules)->validate();

        // saving
        $admin->register($request->input());

        return redirect('/users/view/1');

    }

    public function registerLibrarian (Request $request, $user = 0) {

        $librarian = new Bibliotecario;

        // updating
        if ($user != 0) {
            $librarian->editById($request->input(), $user);
            return redirect('/users/view/2');
        }

        // validating
        $rules = [
            'fullname' => 'required',
            'username' => 'required|unique:bibliotecarios',
            'email' => 'required|email|unique:bibliotecarios',
            'level' => 'required'
        ];

        Validator::make($request->input(), $rules)->validate();

        // saving
        $librarian->register($request->input());

        return redirect('/users/view/2');

    }

    public function registerReader (Request $request, $user = 0) {

        $reader = new Leitor;

        // updating
        if ($user != 0) {
            $reader->editById($request->input(), $user);
            return redirect('/users/view/3');
        }

        // validating
        $rules = [
            'fullname' => 'required',
            'username' => 'required|unique:bibliotecarios',
            'email' => 'required|email|unique:bibliotecarios',
            'groupId' => 'required'
        ];

        Validator::make($request->input(), $rules)->validate();

        // saving
        $reader->register($request->input());

        return redirect('/users/view/3');

    }

}
