<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Isbd2Area extends Model
{

    protected $fillable = [
        'editionStatement',
        'parallelEditionStatement',
        'declarationOfResponsibilityForEdition',
        'additionalEditionInstruction',
        'statementOfResponsibilityAfter'
    ];

    public function register ($data) {

        $this->editionStatement = $data[0];
        $this->parallelEditionStatement = $data[1];
        $this->declarationOfResponsibilityForEdition = $data[2];
        $this->additionalEditionInstruction = $data[3];
        $this->statementOfResponsibilityAfter = $data[4];

        $this->save();
        return $this->id;
    }

    public function retrieveById ($id) {
        return $this->find($id)->get();
    }

    public function deleteById ($id) {
        return $this->destroy($id);
    }

    public function editById ($data, $id) {
        return $this->find($id)->update($data);
    }

}
