<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Isbd1Area extends Model
{

    protected $fillable = [
        'appropriateTitle',
        'parallelTitle',
        'otherTitleInformation',
        'declarationOfResponsibility'
    ];

    public function register ($data) {

        $this->appropriateTitle = $data[0];
        $this->parallelTitle = $data[1];
        $this->otherTitleInformation = $data[2];
        $this->declarationOfResponsibility = $data[3];

        $this->save();
        return $this->id;
    }

    public function retrieveById ($id) {
        return $this->find($id)->get();
    }

    public function deleteById ($id) {
        return $this->destroy($id);
    }

    public function editById ($data, $id) {
        return $this->find($id)->update($data);
    }

}
