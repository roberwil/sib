<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Material extends Model
{

    protected $fillable = [
        'classification',
        'nExemplary',
        'type',
        'registeredBy',
        'title',
        'authors',
        'edition',
        'editionYear',
        'publisher',
        'publishingYear'
    ];

    public static function get () {

        $materials = Material::all();
        $data = [];

        foreach ($materials as $material) {
            $data[$material->id] = $material->title . ' de ' . $material->authors;
        }

        return $data;
    }

    public function retrieveAll () {
        return $this->all();
    }

    public function deleteById ($id) {
        return $this->destroy($id);
    }

    public function editById ($data, $id) {
        return $this->find($id)->update($data);
    }

    public function register ($data) {

        $this->type = $data['type'];
        $this->nExemplary = $data['nExemplary'];
        $this->classification = $data['classification'];

        $this->title = $data['title'];
        $this->authors = $data['authors'];
        $this->edition = $data['edition'];
        $this->editionYear = $data['editionYear'];
        $this->publisher = $data['publisher'];
        $this->publishingYear = $data['publishingYear'];

        if (array_key_exists("registeredBy", $data)) {
            $this->registeredBy = $data['registeredBy'];
        }

        $this->save();

        return $this->id;

    }

}
