<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Prateleira extends Model
{

    protected $fillable = [
        'cdd', 'maxBooks', 'bookcase'
    ];

    public static function blockRegister($id) {
        $shelves = DB::table('prateleiras')->where('bookcase', '=', $id)->get();
        return count($shelves) == Estante::find($id)->nShelves;
    }

    public function retrieveAll () {
        return $this->all();
    }

    public static function getForLocation ($bookcase) {

        $data = DB::table('prateleiras')->where('bookcase', '=', $bookcase)->get();
        $result = [];

        $result['0'] = 'Escolha a prateleira';

        foreach ($data as $item)
            $result[$item->id] = $item->description;

        return $result;

    }

    public function retrieveAllByBookcasId($id) {
        return DB::table('prateleiras')->where('bookcase', '=', $id)->get();
    }

    public function deleteById ($id) {
        return $this->destroy($id);
    }

    public function editById ($data, $id) {
        return $this->find($id)->update($data);
    }

    public function register ($data) {

        $this->cdd = $data['cdd'];
        $this->maxBooks = $data['maxBooks'];
        $this->bookcase = $data['bookcase'];

        return $this->save();

    }

}
