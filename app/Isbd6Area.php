<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Isbd6Area extends Model
{

    protected $fillable = [
        'ownTitleOfMonographicFeatureInSeries',
        'parallelTitleOfMultipartySeries',
        'otherTitleInformationForSerial',
        'declarationOfLiabilityForSerial',
        'internationalStandardNumberOfSerial',
        'numberingWithinSerial'
    ];

    public function register ($data) {

        $this->ownTitleOfMonographicFeatureInSeries = $data[0];
        $this->parallelTitleOfMultipartySeries = $data[1];
        $this->otherTitleInformationForSerial = $data[2];
        $this->declarationOfLiabilityForSerial = $data[3];
        $this->internationalStandardNumberOfSerial = $data[4];
        $this->numberingWithinSerial = $data[5];

        $this->save();
        return $this->id;

    }

    public function retrieveById ($id) {
        return $this->find($id)->get();
    }

    public function deleteById ($id) {
        return $this->destroy($id);
    }

    public function editById ($data, $id) {
        return $this->find($id)->update($data);
    }

}
