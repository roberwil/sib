<?

/** AREA 1 */
$table->string('appropriateTitle');
$table->string('parallelTitle');
$table->string('otherTitleInformation');
$table->string('declarationOfResponsibility');
/** AREA 2 */
$table->string('editionStatement');
$table->string('parallelEditionStatement');
$table->string('declarationOfResponsibilityForEdition');
$table->string('additionalEditionInstruction');
$table->string('statementOfResponsibilityAfter');
/** AREA 3 */
$table->string('mathematicalData');
$table->string('statementOfMusicFormat');
$table->string('numbering');
/** AREA 4 */
$table->string('placeOfPublication');
$table->string('nameOfPublisher');
$table->date('dateOfPublication');
$table->string('placeOfPrinting');
$table->string('printerName');
$table->date('dateOfPrinting');
/** AREA 5 */
$table->string('specificationOfMaterialAndExtension');
$table->string('otherPhysicalDetails');
$table->string('dimensions');
$table->string('declarationOfAccompanyingMaterial');
/** AREA 6 */
$table->string('ownTitleOfMonographicFeatureInSeries');
$table->string('parallelTitleOfMultipartySeries');
$table->string('otherTitleInformationForSerial');
$table->string('declarationOfLiabilityForSerial');
$table->string('internationalStandardNumberOfSerial');
$table->string('numberingWithinSerial');
/** AREA 7 */
$table->string('contentMediaTypeArea');
$table->string('titleDeclarationAreaResponsibility');
$table->string('editionAreaBibliographicHistoryResource');
$table->string('materialTypeResourceSpecificArea');

$table->string('publicationArea');
$table->string('physicalDescriptionArea');
$table->string('seriesArea');
$table->string('content');

$table->string('resourceIdentifierAvailabilityAreaTerms');
$table->string('questionPartIterationBaseDescription');
$table->string('otherNotes');
$table->string('copyingHands');
/** AREA  8 */
$table->string('resourceIdentifier');
$table->string('keyTitle');
$table->string('termsAvailability');
