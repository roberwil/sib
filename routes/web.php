<?php

/** AUTH ROUTES */

Route::get('/', 'LoginController@viewLogin');
Route::get('/login', 'LoginController@viewLogin');
Route::post('/login', 'LoginController@login');
Route::get('/logout', 'LoginController@logout');

/** CONFIG SECTION */

/* Users config */

Route::post('/users/register/admin/{user?}', 'ConfigController@registerAdmin');
Route::post('/users/register/librarian/{user?}', 'ConfigController@registerLibrarian');
Route::post('/users/register/userbib/{user?}', 'ConfigController@registerReader');

Route::get('/users/view/{id}', 'ConfigController@viewUsers');
Route::get('/users/delete/{id}/{user}', 'ConfigController@deleteUsers');

/* Groups config */

Route::post('/groups/register/{id?}', 'ConfigController@registerGroup');

Route::get('/groups/view', 'ConfigController@viewGroups');
Route::get('/groups/delete/{id}', 'ConfigController@deleteGroups');

/* Bookcases, Shelves and Library */

Route::get('/library/bookcases/shelves/view', 'ConfigController@viewShelves');

Route::get('/library/bookcases/view', 'ConfigController@viewBookcases');
Route::get('/library/bookcases/delete/{id?}', 'ConfigController@deleteBookcase');
Route::post('/library/bookcases/register/{id?}', 'ConfigController@registerBookcase');

Route::get('/library/bookcases/shelves/view/{id?}', 'ConfigController@viewShelves');
Route::get('/library/bookcases/shelves/delete/{id}', 'ConfigController@deleteShelf');
Route::post('/library/bookcases/shelves/register/{id?}', 'ConfigController@registerShelf');

Route::get('/library/view', 'ConfigController@viewLibrary');
Route::post('/library/config/{id?}', 'ConfigController@configLibrary');
Route::post('/library/floor/register/{id?}', 'ConfigController@registerFloor');

/** CATALOG ROUTES */

Route::get('/catalog/delete/{which?}/{id?}', 'CatalogController@deleteItem');

Route::get('/catalog/view', 'CatalogController@viewMaterial');
Route::get('/catalog/register/new/view/{step?}', 'CatalogController@viewRegisterNew');

Route::post('/catalog/register/new/{step?}', 'CatalogController@registerNew');
Route::post('/catalog/register/exemplary/{id?}', 'CatalogController@registerExemplary');
Route::get('/catalog/view/exemplary/{material?}', 'CatalogController@viewExemplary');
Route::get('/catalog/view/material', 'CatalogController@viewMaterial');

Route::get('/catalog/view/edit-material/{id?}', 'CatalogController@viewEditMaterial');
Route::post('/catalog/edit/material/{id}', 'CatalogController@editMaterial');

Route::get('/catalog/search', 'CatalogController@viewSearch');

/** CIRCULATION SECTION */

Route::get('/circulation/view-borrow', 'CirculationController@viewBorrow');
Route::get('/circulation/view-deliver', 'CirculationController@viewDeliver');

/** STATS SECTION */

Route::get('/stats/view', 'CirculationController@viewStats');

/** HELPER ROUTES */

Route::get('/default', 'HelperController@defaultPage');
Route::get('helper/lines/{floorId}', 'HelperController@getLinesByFloorId');
Route::get('helper/get-cdd/{level}', 'HelperController@getCDD');
Route::get('/test/{text}', "HelperController@test");
Route::get('helper/exemplary-location/{which?}/{id?}/{helperId?}', 'HelperController@exemplaryLocationHandler');
Route::get('/helper/check/book/{id}', 'HelperController@checkBook');
Route::get('/helper/check/user/{id}', 'HelperController@checkUser');




